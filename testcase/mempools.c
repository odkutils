#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>

int
main (int argc, char **argv)
{
	int i;
	void *ptr, *ptr2;
	odkMemPool *pool = odk_mempool_new (1, 100);

	for (i = 0; i < 16; i++) {
		ptr = odk_mempool_alloc (pool, 1);
	}

	for (i = 0; i < 16; i++) {
		ptr2 = odk_mempool_alloc (pool, 1);
	}

	odk_mempool_dealloc (pool, ptr);
	odk_mempool_dealloc (pool, ptr2);

	ptr2 = odk_mempool_alloc (pool, 1);
	ptr = odk_mempool_alloc (pool, 1);

	odk_mempool_free (pool);

	return 0;
}
