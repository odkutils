#include <stdio.h>
#include <odkutils.h>

int
main (int argc, char *argv)
{
	odk_log_init ("logs.txt", LOG_DEBUG, LOG_EXIT_ONERROR);
	odk_log_warning ("Example Warning");
	odk_log_debug ("Example debug message");
	odk_log_error ("Fatal error log message!");
	return 0;
}
