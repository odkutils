#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

#include <odkutils.h>

int icmp(int n1, int n2){
	return n1 - n2;
}

int main(int argc, char **argv){
	odkList *list;
	int i, n;
	
	list = odk_list_new(1500);
	
	printf("Creating number list\n");
	for (i = 0; i < 1500; i++){
		n = rand();
		odk_list_append(list, (void*)n);
	}
	
	printf("Sorting\n");
	odk_list_quicksort(list, (CMP_FUNC)&icmp);
	
	printf("Freeing Memory\n");
	odk_list_free(list, NULL);
	
	return 0;
}
