/*
odkUtils: Utility Library
        Shell-Like navigation on odkUtils Config Store
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>
#include <odkutils_xml.h>

odkConfigStore *conf = NULL;
char path[1024];
char tmpbuff[1024];

int
readline (char *str)
{
	int c;
	int i = 0;
	while ((c = getchar ()) != '\n') {
		str[i] = c;
		i++;
	}

	str[i] = 0;
}

void
help ()
{
	printf ("odkUtils ConfigStore Shell\n");
	printf ("Available Commands\n");
	printf ("  ls            - List directory contents\n");
	printf ("  cd [dir]      - Change directory to dir\n");
	printf ("  cat [key]     - Show key contents\n");
	printf ("  mkdir [dir]   - Creates directory dir in current path\n");
	printf ("  mkentry [key] - Creates key and asks for initial value\n");
	//printf("  edentry [key] - Edits key value\n");
	printf ("  help          - Show this help\n");
	printf ("  quit          - Exit shell\n\n");
}

void
join_path (char *path2)
{
	if (strlen (path) == 1) {
		sprintf (tmpbuff, "%s%s", path, path2);
	} else {
		sprintf (tmpbuff, "%s/%s", path, path2);
	}
}

void
mk_dir (char *dir)
{
	if (strlen (dir) < 2)
		return;

	join_path (dir);

	if (odk_config_key_type (conf, tmpbuff) != -1) {
		printf ("Error, %s already exists!\n");
		return;
	}

	odk_config_key_set (conf, tmpbuff, NULL);
}

void
mk_entry (char *dir)
{
	char ibuff[500];

	if (strlen (dir) < 2)
		return;

	join_path (dir);

	if (odk_config_key_type (conf, tmpbuff) != -1) {
		printf ("Error, %s already exists!\n");
		return;
	}

	memset (ibuff, 0, 500);
	printf ("Value: ");
	readline (ibuff);

	odk_config_key_set (conf, tmpbuff, ibuff);
}

void
cat_key (char *dir)
{
	char *p;
	int t;

	if (strlen (dir) < 2)
		return;

	if (strlen (path) == 1) {
		sprintf (tmpbuff, "%s%s", path, dir);
	} else {
		sprintf (tmpbuff, "%s/%s", path, dir);
	}

	t = odk_config_key_type (conf, tmpbuff);

	if ((t == KEY_DIR) || (t == -1))
		return;

	p = odk_config_key_get (conf, tmpbuff);

	if (p != NULL)
		printf ("%s\n", p);
}

void
change_dir (char *dir)
{
	char *p;

	if (!strcmp (dir, "..")) {

		p = rindex (path, '/');
		if (p != path) {
			*p = 0;
		} else {
			p[1] = 0;
		}

		return;
	}

	if (strlen (path) == 1) {
		sprintf (tmpbuff, "%s%s", path, dir);
	} else {
		sprintf (tmpbuff, "%s/%s", path, dir);
	}

	if (odk_config_key_type (conf, tmpbuff) != KEY_DIR) {
		printf ("%s is not a directory!\n");
		return;
	}

	sprintf (path, tmpbuff);
}

void
list_dir ()
{
	odkSList *l;

	l = odk_config_key_list (conf, path);

	printf ("Listing for %s\n", path);
	for_slist (l) {
		printf ("%s\n", odk_slist_data (l));
	}

	odk_slist_free (l, &free);
}

int
enter_shell ()
{
	int onRun = 1;
	char buff[200];

	sprintf (path, "/");

	while (onRun) {
		printf ("[%s] ", path);
		memset (buff, 0, 200);
		readline (buff);

		switch (buff[0]) {
		case 'l':
			list_dir ();
			break;
		case 'h':
			help ();
			break;
		case 'c':
			if ((buff[1] == 'a') && (buff[2] == 't')) {
				if (strlen (buff) > 3)
					cat_key (strstr (buff, " ") + 1);
				break;
			}
			if (strlen (buff) > 3)
				change_dir (strstr (buff, " ") + 1);
			break;
		case 'm':
			if (!strncmp (buff, "mkdir", 5)) {
				if (strlen (buff) > 5)
					mk_dir (strstr (buff, " ") + 1);
			} else if (!strncmp (buff, "mkentry", 7)) {
				if (strlen (buff) > 7)
					mk_entry (strstr (buff, " ") + 1);
			}
			break;
		case 'q':
			onRun = 0;
			break;
		case 0:
		case '\n':
			break;
		default:
			printf ("Unknown Command\n");
			break;
		}
	}

	return 0;
}

int
main (int argc, char **argv)
{
	int r;

	if (argc == 1)
		return 1;

	printf ("Loading Shell on %s\n", argv[1]);
	conf = (odkConfigStore *) odk_xmlconfig_new (argv[1], "config");
	if (!odk_config_load (conf)) {
		printf ("Error, unable to load configuration\n");
		object_unref (conf);
		return 1;
	}
	printf ("Seccess!\n");

	r = enter_shell ();

	odk_config_save (conf);
	object_unref (conf);
	return r;
}
