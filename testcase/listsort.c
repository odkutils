#include <stdio.h>
#include <string.h>

#include <odkutils.h>


//typedef int (ODKAPI * CMP_FUNC) (void *data1, void *data2);
odkList *tmp;

void print_stat(){
	int n;
	printf("Status: ");
	for LIST_ITERATOR(n, tmp, int){
		printf("%i, ", n);
	}
	printf("\n");
}

#ifdef CUSTOM

int odk_list_swap2(odkList *list, odkNode *n1, odkNode *n2){
	void *d;
	
	d = n2->data;
	n2->data = n1->data;
	n1->data = d;
	
	return 0;
}

odkNode *odk_list_node(odkList *list){
	return list->current;
}

int odk_list_swap(odkList *list, odkNode *n1, odkNode *n2){
	odkNode *ptr;
	
	if (n1->next == n2){
		n1->next = n2->next;
		n2->next = n1;
		
		n2->prev = n1->prev;
		n1->prev = n2;
	}else if (n2->next == n1){
		printf("Unhandled swap case! n2->next == n1\n");
	}else{
		ptr = n1->next;
		n1->next = n2->next;
		n2->next = ptr;

		ptr = n1->prev;
		n1->prev = n2->prev;
		n2->prev = ptr;
	}
	
	if (n1->next){
		n1->next->prev = n1;
	}else{
		list->last = n1;
	}
	if (n1->prev){
		n1->prev->next = n1;
	}else{
		list->first = n1;
	}
	
	if (n2->next){
		n2->next->prev = n2;
	}else{
		list->last = n2;
	}
	if (n2->prev){
		n2->prev->next = n2;
	}else{
		list->first = n2;
	}
}

int partition(odkList *list, odkNode *s, odkNode *e, CMP_FUNC ocmp){
	int pindex;
	odkNode *pivot, *el1, *el2, *el3;
	
	if (s->next == e){
		if (ocmp(s->data, e->data) == 1){
			odk_list_swap2(list, s, e);
		}
		return 0;
	}
	//TODO: Choose a better pivot.
	pivot = e;
	
	el1 = s;
	
	while(1){
		el3 = el1->next;
		if (ocmp(el1->data, pivot->data) == 1){
			el2 = el1->next;
			
			while((ocmp(el1->data, el2->data) != 1) && (el2 != pivot)){
				el2 = el2->next;
			}
			
			odk_list_swap2(list, el1, el2);
			
			//Pivot is in the final place
			if (el2 == pivot){
				pivot = el1;
				if ((pivot->next != list->last) && (pivot->next != e)){
					partition(list, pivot->next, el2, ocmp);
				}
				if ((pivot->prev != list->first) && (pivot->prev != s)){
					partition(list, list->first, pivot->prev, ocmp);
				}
				return 0;
			}
			el3 = el1;
		}
		
		if ((el3 == e) || (el3 == NULL)){
			return 0;
		}
		
		el1 = el3;
	}
}

int odk_list_quicksort(odkList *list, CMP_FUNC ocmp){
	partition(list, list->first, list->last, ocmp);
}

#endif
odkList *odk_list_new_fromarray(void **array, int n){
	int i;
	odkList *list;
	
	list = odk_list_new(n);
	for (i = 0; i < n; i++){
		odk_list_append(list, array[i]);
	}
	
	return list;
}

int nums[30] = {3, 83, 57, 29, 48, 45, 44, 23, 35, 21, 1, 30, 23, 23, 45,
				10, 6, 78, 32, 46, 2, 334, 56, 34, 342, 2, 12, 23, 34, 90};

int mycmp(int n1, int n2){
	return n1-n2;
	
	int r;
	
	//r = n1 - n2;
	
	if (n1 > n2){
		return 1;
	}else if (n1 < n2){
		return -1;
	}
	return 0;
}

int main(int argc, char **argv){
	tmp = odk_list_new_fromarray((void**)nums, 30);
	print_stat();
	odk_list_quicksort(tmp, (CMP_FUNC)&mycmp);
	print_stat();
	odk_list_free(tmp, NULL);
}
