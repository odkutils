#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>

typedef struct {
	CLASS_OBJECT
	int number;
	char buffer[80];
} MyObject;

void myobject_del(void *o){
	printf("Object Deletion!\n");
}

MyObject *myobject_new(){
	MyObject *mo;

	mo = malloc(sizeof(MyObject));
	object_register(mo, "MyObject");
	
	mo->number = 0;
	memset(mo->buffer, 0, 80);
	object_append_destructor(mo, (FREE_FUNC)&myobject_del);

	return mo;
}

void *thread(void *obj){
	MyObject *mo;
	int i;

	mo = (MyObject*)obj;
	sleep(1);
	for (i = 0; i < 1000; i++){
		printf("Thread 1 Looping!\n");
		//object_lock(mo);
		mo->number = 2;
		sprintf(mo->buffer, "Hello from thread 1");
		//object_unlock(mo);
		//sleep(1);
	}
	printf("Returning from Thread 1\n");
}

void *thread2(void *obj){
	MyObject *mo;
	int i;

	mo = (MyObject*)obj;
	sleep(2);
	for (i = 0; i < 1000; i++){		
		printf("Thread 2 Looping!\n");
		//object_lock(mo);
		mo->number = 100;
		sprintf(mo->buffer, "Hello from thread 2");
		//object_unlock(mo);
		//sleep(1);
	}
	printf("Returning from Thread 2\n");
}

int main(int argc, char **argv){
	odkThread *t1;
	odkThread *t2;
	MyObject *mo;
	int i;

	mo = myobject_new();

	t2 = odk_thread_create(&thread2, mo);
	sleep(1);
	t1 = odk_thread_create(&thread, mo);
	sleep(1);

	//odk_thread_detach(t1);
	//odk_thread_detach(t2);

	for (i = 0; i < 1000; i++){
		//object_lock(mo);
		printf("String: %s\tNumber: %i\n", mo->buffer, mo->number);
		//object_unlock(mo);
		//sleep(1);
	}

	odk_thread_join(t1);
	odk_thread_join(t2);

	object_unref(mo);

	return 0;
}
