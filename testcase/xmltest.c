#include <stdio.h>
#include <odkutils.h>
#include <odkutils/xml.h>

int
main (int argc, char *argv)
{
	odkXMLNode *r = odk_xml_node_new (NULL, "config");
	odkXMLNode *c;

	odk_xml_attrib_set (r, "version", "3.4");

	c = odk_xml_node_new (r, "host");
	c = odk_xml_node_new (c, "lanhost");
	odk_xml_data_set (c, "localhost");

	c = odk_xml_node_new (r, "port");
	odk_xml_data_set (c, "3960");

	c = odk_xml_node_new (r, "user");
	odk_xml_data_set (c, "root");

	c = odk_xml_node_new (r, "db");
	odk_xml_data_set (c, "produce");

	odk_xml_write ("test.xml", r);

	c = odk_xml_child_get (r, "port");

	printf ("Port: %s\n", odk_xml_data_get (c));

	odk_xml_free (r);

	return 1;
}
