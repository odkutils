#include <odkutils.h>
#include <odkutils_xml.h>

int
main (int argc, char **argv)
{
	odkConfigStore *conf =
		(odkConfigStore *) odk_xmlconfig_new ("test.conf", "config");

	odk_config_key_set (conf, "/user/name", "Anonymous");
	odk_config_key_set (conf, "/user/initials", "ANON");
	odk_config_key_set (conf, "/user/mail", "anonymous@site.com");
	odk_config_key_set (conf, "/user/signature", "Woot!");
	odk_config_key_set (conf, "/prefs/showmail", "true");
	odk_config_key_set (conf, "/prefs/sign", "false");
	odk_config_save (conf);

	object_unref (conf);
}
