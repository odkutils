#include <stdio.h>
#include <odkutils.h>
#include <odkutils_xml.h>

int
main (int argc, char **argv)
{
	printf ("Root\n");
	odk_xml_path (NULL, "/");

	printf ("\nOne\n");
	odk_xml_path (NULL, "/mysql");

	printf ("\nTwo\n");
	odk_xml_path (NULL, "/mysql/meta");

	printf ("\nTree\n");
	odk_xml_path (NULL, "/mysql/meta/author");

	printf ("\nBig Path\n");
	odk_xml_path (NULL, "/this/is/a/big/path/for/an/xml/file");

	return 0;
}
