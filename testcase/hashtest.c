#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>

char *
strcopy (char *string)
{
	int ilen = strlen (string);
	char *rets = (char *) malloc (ilen + 1);
	memset (rets, 0, ilen + 1);
	memcpy (rets, string, ilen);

	return rets;
}

char *
genstr (int maxsize)
{
	int s, i;
	char *r;

	s = rand () % maxsize;
	r = (char *) malloc (s);

	for (i = 0; i < s; i++) {
		r[i] = 48 + (rand () % 48);
	}

	return r;
}

int
main (int argc, char **argv)
{
	odkHashTable *ht = odk_hashtable_new (0);
	char *t;
	int i, tmp;
	char *keydict[50];

	for (i = 0; i < 50; i++) {
		keydict[i] = genstr (256);
	}

	for (i = 0; i < 64000; i++) {
		tmp = rand () % 48;

		odk_hashtable_insert (ht, keydict[tmp], NULL);
	}

	for (i = 0; i < 64000; i++) {
		t = odk_hashtable_lookup (ht, "jdjkder8938934hruihefhifjhidjd");
		t = odk_hashtable_lookup (ht, "carlos");
	}

	odk_hashtable_free (ht, NULL);

	for (i = 0; i < 50; i++) {
		free (keydict[i]);
	}
}
