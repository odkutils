#include <odkutils.h>
#include <odkutils_xml.h>

int
main (int argc, char **argv)
{
	odkConfigStore *conf =
		(odkConfigStore *) odk_xmlconfig_new ("test.conf", "config");

	odk_config_load (conf);
	odk_config_key_set (conf, "/user/initials", "TEST");
	odk_config_key_set (conf, "/user/mail", "anonymous@site.com");
	odk_config_key_set (conf, "/prefs/showmail", "true");
	odk_config_key_set (conf, "/prefs/sign", "true");
	odk_config_save (conf);

	object_unref (conf);
}
