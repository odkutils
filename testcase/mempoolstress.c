#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/bitfield.h>
#include <odkutils/mempool.h>
#include <odkutils/list.h>

int
main ()
{
	int i;
	odkList *l = odk_list_new (100);
	void *d;
	int c;

	for (i = 0; i < 100; i++) {
		odk_list_append (l, NULL);
	}

	for (i = 0; i < 100; i++) {
		odk_list_preappend (l, NULL);
	}

	odk_list_first (l);
	for (i = 0; i < 200; i++) {
		c = rand () % 2;

		if (c == 1)
			odk_list_delete (l);

		odk_list_next (l);
	}

	for (i = 0; i < 100; i++) {
		odk_list_append (l, NULL);
	}

	while (odk_list_first (l)) {
		odk_list_delete (l);
	}

	for (i = 0; i < 299; i++) {
		odk_list_append (l, NULL);
	}

	odk_list_free (l, NULL);
}
