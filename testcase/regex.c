
#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>

char *validate_test1 = "01-800-324-521-32-32";
char *validate_test2 = "01-80-3424-51-32-322";
char *find_test = "ksak(sksa)sak(k)(32)kdskds";
char *find_all_test = "insert into test values(:name:, :address:, :phone:)";

char *validate_regex = "\\d\\d-\\d\\d\\d-\\d\\d\\d-\\d\\d\\d-\\d\\d-\\d\\d";
char *find_regex = "\\((\\d+)\\)";
char *findall_regex = ":(\\w+):";

int main(int argc, char **argv){
	odkRegex *re;
	odkRegMatch *m;
	odkSList *l;
	char buffer[50];
	int i;
	
	re = odk_regex_new(validate_regex, 0);
	
	printf("Doing Validation Tests:\n");
	printf("  * String 1: ");
	if (odk_regex_validate(re, validate_test1)){
		printf("PASS\n");
	}else{
		printf("FAIL\n");
	}
	
	printf("  * String 2: ");
	if (odk_regex_validate(re, validate_test2)){
		printf("FAIL\n");
	}else{
		printf("PASS\n");
	}
	
	odk_regex_free(re);
	
	printf("Doing Find Tests:\n");
	re = odk_regex_new(find_regex, 0);
	
	printf("  * Find (\\b+): ", find_test);
	m = odk_regex_match(re, find_test, 0);
	if (m == NULL){
		printf("FAIL\n");
	}else{
		printf("PASS\n");
		
		memset(buffer, 0, 50);
		memcpy(buffer, (char*)(find_test+ m->start), m->end - m->start);
		printf("    - Match[%i:%i]: %s\n", m->start, m->end, buffer);
		
		printf("    Submatches: %i\n", m->count);
		for (i = 0; i < m->count; i++){
			memset(buffer, 0, 50);
			memcpy(buffer, (char*)(find_test+ m->submatch[i].start), m->submatch[i].end - m->submatch[i].start);
			printf("    - SubMatch[%i:%i]: %s\n", m->submatch[i].start, m->submatch[i].end, buffer);
		}
		odk_match_free(m);
	}
	
	odk_regex_free(re);
	
	printf("Doing Find All Tests:\n");
	re = odk_regex_new(findall_regex, 0);
	
	printf("  * Find all: ");
	
	l = odk_regex_match_all(re, find_all_test);
	
	if (odk_slist_count(l) == 0){
		printf("FAIL\n");
	}else{
		printf("PASS\n");
		
		for SLIST_ITERATOR(m, l, odkRegMatch*){
			memset(buffer, 0, 50);
			memcpy(buffer, (char*)(find_all_test+ m->start), m->end - m->start);
			printf("    - Match[%i:%i]: %s\n", m->start, m->end, buffer);
			
			printf("    Submatches: %i\n", m->count);
			for (i = 0; i < m->count; i++){
				memset(buffer, 0, 50);
				memcpy(buffer, (char*)(find_all_test+ m->submatch[i].start), m->submatch[i].end - m->submatch[i].start);
				printf("    - SubMatch[%i:%i]: %s\n", m->submatch[i].start, m->submatch[i].end, buffer);
			}
		}
	}
	
	odk_slist_free(l, (FREE_FUNC)&odk_match_free);
	odk_regex_free(re);
	
	return 0;
}
