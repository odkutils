#include <stdio.h>
#include <odkutils.h>
#include <odkutils/xml.h>

int
main (int argc, char *argv)
{
	odkXMLNode *r;
	odkXMLNode *c;

	r = odk_xml_parse ("test.xml");

	printf ("Version: %s\n", odk_xml_attrib_get (r, "version"));

	c = odk_xml_child_get (r, "port");

	printf ("  Port: %s\n", odk_xml_data_get (c));

	odk_xml_free (r);

	return 1;
}
