#include <stdio.h>
#include <malloc.h>

#include <odkutils.h>
#include <odkutils_object.h>

//Declare Instance Members
typedef struct {
	char *data;
} _MyObject;

//Define Class and hierarchy
#define CLASS_MYOBJECT CLASS_OBJECT _MyObject Class_MyObject;

//Define MyObject Class
typedef struct {
CLASS_MYOBJECT} MyObject;

//Instance Member Accessor
#define MYOBJECT(o) (o->Class_MyObject)

//MyObject Destructor
void
myobject_delete (MyObject * o)
{
	if (MYOBJECT (o).data != NULL) {
		free (MYOBJECT (o).data);
	}
}

void *
myobject_new ()
{
	MyObject *o = (MyObject *) malloc (sizeof (MyObject));

	//Register MyObject
	object_register (o, "MyObject", &myobject_delete);

	//Initialize MyObject members
	MYOBJECT (o).data = malloc (32);

	return o;
}

int
main (int argc, char **argv)
{
	MyObject *obj;
	odkSList *l = odk_slist_new (0);
	int i;

	//Create 300 MyObjects and store them in a list 
	for (i = 0; i < 300; i++) {
		obj = myobject_new ();
		odk_slist_append (l, obj);
	}

	//Free the list, and free the objects in it
	odk_slist_free (l, &object_unref);

	return 0;
}
