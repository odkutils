/*
odkUtils: Utility Library
        Cross-Platform Mutex, Win32 Specific Code
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <windows.h>

#include <odkutils/mutex.h>

struct odkMutex_s{
	HANDLE mtx;
};

odkMutex *odk_mutex_new (){
	odkMutex *mutex;

	mutex = (odkMutex*)malloc(sizeof(odkMutex));

	mutex->mtx = CreateMutex(NULL, FALSE, NULL);
	return mutex;
}

void odk_mutex_destroy (odkMutex * mutex){
	if (mutex == NULL)
		return;

	CloseHandle(mutex->mtx);
	free(mutex);
}

void odk_mutex_lock (odkMutex * mutex){
	if (mutex == NULL)
		return;

	WaitForSingleObject(mutex->mtx, INFINITE);
}

int odk_mutex_trylock (odkMutex * mutex){
	DWORD ret;
	
	if (mutex == NULL)
		return 0;

	ret = WaitForSingleObject(mutex->mtx, 0);

	if (ret == WAIT_OBJECT_0)
		return 1;

	return 0;
}

void odk_mutex_unlock (odkMutex * mutex){
	if (mutex == NULL)
		return;
	
	ReleaseMutex(mutex->mtx);
}
