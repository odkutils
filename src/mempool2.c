/*
odkUtils: Utility Library
        Memory Pools
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/bitfield.h>
#include <odkutils/mempool2.h>

odkMemPool *
odk_mempool_new (int objSize, int maxCount)
{
	odkMemPool *pool;

	pool = (odkMemPool *) malloc (sizeof (odkMemPool));
	pool->objSize = objSize;
	pool->maxCount = maxCount;
	pool->objCount = 0;
	pool->allocCount = 0;
	pool->next = NULL;
	pool->memPool = NULL;
	pool->allocMap = odk_bitfield_new (maxCount);
	pool->refCount = 1;
}

void
odk_mempool_free (odkMemPool * pool)
{
	odkMemPool *p;

	if (pool == NULL) {
		printf ("No pool to free\n");
		return;
	}

	pool->refCount--;

	if (pool->refCount > 0)
		return;

	p = pool;

	do {
		printf ("Freeing pool memory\n");
		if (p->memPool)
			free (p->memPool);
		free (p->allocMap);
		p = p->next;
	} while (p != NULL);

	do {
		printf ("Freeing memory pool lists\n");
		p = pool;
		pool = pool->next;

		free (p);
	} while (pool != NULL);
}

void *
odk_mempool_alloc (odkMemPool * pool, int objSize)
{
	void *raddr;
	int n;

	if (pool == NULL) {
		return malloc (objSize);
	}

	while (pool->objCount == pool->maxCount) {
		pool = pool->next;
	}

	if (pool->memPool == NULL) {
		pool->objCount = 0;
		pool->memPool = malloc (pool->objSize * pool->maxCount);

		if (pool->memPool == NULL)
			printf ("ERROR: Unable to alloc mempool of size (%i * %i)",
					pool->objSize, pool->maxCount);
	}

	if (pool->allocCount < pool->maxCount) {
		n = pool->allocCount;
	} else if (pool->objCount > pool->maxCount) {
		n = odk_bitfield_next_unset (pool->allocMap);
	}

	odk_bitfield_set (pool->allocMap, n);
	raddr = pool->memPool + ((n - 1) * pool->objSize);
	pool->objCount++;
	pool->allocCount++;

	if ((pool->objCount == pool->maxCount) && (pool->next == NULL)) {
		printf ("Allocating new page\n");
		pool->next = (odkMemPool *) malloc (sizeof (odkMemPool));
		pool->next->objSize = pool->objSize;
		pool->next->maxCount = pool->maxCount;

		pool = pool->next;

		pool->objCount = 0;
		pool->memPool = NULL;
		pool->allocMap = odk_bitfield_new (pool->maxCount);
		pool->next = NULL;
	}
	//printf("Allocating object %i, count: %i\n", n, pool->objCount);       

	return raddr;
}

void
odk_mempool_dealloc (odkMemPool * pool, void *ptr)
{
	int n;

	if (pool == NULL) {
		free (ptr);
		return;
	}

	do {
		if (((int) ptr >= (int) pool->memPool)
			&& ((int) ptr <=
				((int) pool->memPool + (pool->objSize * pool->maxCount)))) {
			n = ((int) ptr - (int) pool->memPool) / pool->objSize;
			n++;
			printf ("dealloced %i, count: %i\n", n, pool->objCount);
			odk_bitfield_unset (pool->allocMap, n);
			pool->objCount--;

			if (pool->objCount == 0) {
				free (pool->memPool);
				pool->memPool = NULL;
			}

			return;
		}
	} while (pool = pool->next);
}
