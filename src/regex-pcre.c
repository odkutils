/*
odkUtils: Utility Library
        Regular Expresions
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>
#include <odkutils/regex.h>

#include <pcre.h>

struct odkRegex_s {
	pcre *re;
	pcre_extra *extra;
	int bsize;
	int *buffer;
};

odkRegex *odk_regex_new(char *pattern, int options, int scount){
	odkRegex *re;
	char *errptr;
	int eoffset, noptions;
	
	noptions = 0;
	
	if (options & REGEX_MULTILINE)
		noptions |= PCRE_MULTILINE;
	
	if (options & REGEX_CASELESS)
		noptions |= PCRE_CASELESS;
	
	if (options & REGEX_DOTALL)
		noptions |= PCRE_DOTALL;
	
	if (options & REGEX_NEWLINE_CR)
		noptions |= PCRE_NEWLINE_CR;
	
	if (options & REGEX_NEWLINE_CRLF)
		noptions |= PCRE_NEWLINE_CRLF;	
	
	if (options & REGEX_NEWLINE_LF)
		noptions |= PCRE_NEWLINE_LF;
	
	/*if (options & REGEX_NEWLINE_ANY)
		noptions |= PCRE_NEWLINE_ANY;
	
	if (options & REGEX_NEWLINE_ANYCRLF)
		noptions |= PCRE_NEWLINE_ANYCRLF;*/
	
	re = (odkRegex *)malloc(sizeof(odkRegex));
	re->re = pcre_compile(pattern, noptions,(const char**)&errptr,&eoffset, NULL);
	
	if (options & REGEX_STUDY){
		re->extra = pcre_study((const pcre*) re->re, 0, NULL);
	}else{
		re->extra = NULL;
	}
	
	if (scount > 5){
		re->bsize = scount * 5;
		re->buffer = malloc(sizeof(int) * re->bsize);
	}else{
		re->bsize = 36;
		re->buffer = malloc(sizeof(int) * re->bsize);
	}
	
	return re;
}

void odk_regex_free(odkRegex *re){
	free(re->buffer);
	pcre_free(re->re);
	free(re);
}

void odk_match_free(odkRegMatch *m){
	if (m->count)
		free(m->submatch);
	free(m);
}

odkRegMatch *odk_regex_match(odkRegex *re, char *str, int pos){
	odkRegMatch *ret;
	int r, i;
	
	ret = NULL;
	
	r = pcre_exec(re->re, re->extra, str, strlen(str), pos, 0, re->buffer, re->bsize);
	if (r > 0){
		ret = (odkRegMatch *)malloc(sizeof(odkRegMatch));
		ret->start = re->buffer[0];
		ret->end = re->buffer[1];
		ret->count = 0;
		ret->submatch = NULL;
		
		if (r > 1){
			ret->count = r-1;
			ret->submatch = (odkRegSubMatch*)malloc(sizeof(odkRegSubMatch) * (r-1));
			for (i = 0; i < ret->count ; i++){
				ret->submatch[i].start = re->buffer[2 + (i*2)];
				ret->submatch[i].end = re->buffer[3 + (i*2)];
			}
		}
	}
	
	return ret;
}

odkSList *odk_regex_match_all(odkRegex *re, char *str){
	odkSList *ret;
	odkRegMatch *match;
	int pos, slen, r, i;
	
	ret = odk_slist_new(0);
	
	slen = strlen(str);
	pos = 0;
	
	while((r = pcre_exec(re->re, re->extra, str, slen, pos, 0, re->buffer, re->bsize)) > 0){
		match = (odkRegMatch *)malloc(sizeof(odkRegMatch));
		match->start = re->buffer[0];
		match->end = pos = re->buffer[1];
		match->count = 0;
		match->submatch = NULL;
			
		if (r > 1){
			match->count = r-1;
			match->submatch = (odkRegSubMatch*)malloc(sizeof(odkRegSubMatch) * (r-1));
			for (i = 0; i < match->count ; i++){
				match->submatch[i].start = re->buffer[2 + (i*2)];
				match->submatch[i].end = re->buffer[3 + (i*2)];
			}
		}
		
		odk_slist_append(ret, match);
	}
	
	return ret;
}

int odk_regex_validate(odkRegex *re, char *str){
	int r;
	
	r = pcre_exec(re->re, re->extra, str, strlen(str), 0, 0, re->buffer, re->bsize);
	
	if (r < 1)
		return 0;
	
	return 1;
}

char *odk_submatch_copy(char *str, odkRegMatch *m, int n){
    char *ret;
    int l;
    
    if (m == NULL)
        return NULL;
        
    if (n >= m->count)
        return NULL;
    
    l = m->submatch[n].end - m->submatch[n].start;
    if (l < 1)
        return NULL;
    
    ret = (char*)malloc(sizeof(char)* l + 1);

    memset(ret, 0, l+1);
    memcpy(ret, (char*)((int)str + m->submatch[n].start), l);
    
    return ret;
}
