#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>
//#include <odkutils_xml.h>

#include <odkutils/appconfig.h>

#ifdef WIN32
#else
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#endif

odkConfigStore *odk_appconfig_new(char *app){
	char buffer[120];
	odkLibHandle *lib;
	
#ifdef WIN32
	return NULL;
#else
	DIR *d;
	odkConfigStore* (*xmlconfig_new)(char *b, char *c);
	
	lib = odk_library_open("odkutils_xml");
	xmlconfig_new = odk_library_symbol(lib, "odk_xmlconfig_new");
	
	sprintf(buffer, "%s/.%s", odk_path_user_home(), app);
	
	d = opendir(buffer);
	
	if (d == NULL)
		mkdir(buffer, S_IRUSR | S_IWUSR | S_IXUSR);
	
	closedir(d);
	
	sprintf(buffer, "%s/.%s/%s.conf", odk_path_user_home(), app, app);
	
	return xmlconfig_new(buffer, app);
	
#endif
}

odkConfigStore *odk_appconfig_system_new(char *app){
	char buffer[120];
	
#ifdef WIN32
	return NULL;
#else

	/* TODO: do some checks before opening file */
	sprintf(buffer, "/etc/%s.conf", app);
	
	/*return (odkConfigStore*)odk_xmlconfig_new(buffer, app);*/
	
#endif
}
