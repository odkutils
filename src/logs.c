/*
odkUtils: Utility Library
        Logging Facility
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <odkutils/logs.h>

static char odk_loglevel = 0;
static char odk_logflags = 0;
static FILE *odk_logdest = NULL;

void
odk_log_stop ()
{
	if (odk_logdest != stdout)
		fclose (odk_logdest);
	odk_loglevel = 0;
}

void
odk_log_init (char *file, char level, char flags)
{
	odk_loglevel = level;
	odk_logflags = flags;

	if (file == NULL) {
		odk_logdest = stdout;
		return;
	}
	odk_logdest = fopen (file, "a+");
	//atexit(&odk_log_stop);        
}

void
odk_log_level (char level)
{
	odk_loglevel = level;
}

void
odk_log_print (char *str)
{
}

void
odk_log_printf (char *format, ...)
{

}

void
odk_log_debug (char *format, ...)
{
	va_list args;

	if (odk_loglevel < 3)
		return;

	fprintf (odk_logdest, "[DEBUG] ");
	va_start (args, format);
	vfprintf (odk_logdest, format, args);
	va_end (args);

	fprintf (odk_logdest, "\n");
}

void
odk_log_warning (char *format, ...)
{
	va_list args;

	if (odk_loglevel < 2)
		return;

	fprintf (odk_logdest, "[WARNING] ");
	va_start (args, format);
	vfprintf (odk_logdest, format, args);
	va_end (args);

	fprintf (odk_logdest, "\n");
}

void
odk_log_error (char *format, ...)
{
	va_list args;

	if (odk_loglevel == 0)
		return;

	fprintf (odk_logdest, "[ERROR] ");
	va_start (args, format);
	vfprintf (odk_logdest, format, args);
	va_end (args);

	fprintf (odk_logdest, "\n");

	if (odk_logflags & LOG_EXIT_ONERROR)
		exit (1);
}
