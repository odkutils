/*
odkUtils: Utility Library
        XML parser based on expat
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/logs.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>
#include <odkutils/hashtable.h>
#include <odkutils/xml.h>

#include <expat.h>

#define BUFFSIZE 1024
char buff[BUFFSIZE];

struct ParserData {
	odkXMLNode *r;
};

void
cdata_start (void *userData)
{
	struct ParserData *pd = userData;

	pd->r->cdata = 1;
}

void
cdata_end (void *userData)
{

}

void
el_start (void *data, const char *el, const char **attr)
{
	struct ParserData *pd = data;
	int i;

	if (pd->r == NULL) {
		pd->r = odk_xml_node_new (NULL, (char *) el);
	} else {
		pd->r = odk_xml_node_new (pd->r, (char *) el);
	}

	for (i = 0; attr[i]; i += 2) {
		odk_xml_attrib_set (pd->r, (char *) attr[i], (char *) attr[i + 1]);
	}
}

void
el_data (void *data, const char *el, int len)
{
	struct ParserData *pd = data;
	char *tmp;
	int len1;

	if (len == 1)
		return;

	if (pd->r->data != NULL) {
		len1 = strlen (pd->r->data);
		tmp = (char *) malloc (len1 + len + 2);
		memcpy (tmp, pd->r->data, len1);
		memcpy ((tmp + len1), el, len);
		tmp[len1 + len] = 0;
		free (pd->r->data);
		pd->r->data = tmp;
	} else {
		pd->r->data = (char *) malloc (len + 2);
		pd->r->data[len] = 0;
		strncpy (pd->r->data, el, len);
	}
}

void
el_end (void *data, const char *el)
{
	struct ParserData *pd = data;

	if (pd->r->parent == NULL)
		return;

	pd->r = pd->r->parent;
}

odkXMLNode *
odk_xml_parse (char *file)
{
	FILE *fd;
	XML_Parser p = XML_ParserCreate (NULL);
	struct ParserData pd;

	XML_SetCharacterDataHandler (p, (XML_CharacterDataHandler)el_data);
	XML_SetElementHandler (p, (XML_StartElementHandler)el_start, (XML_EndElementHandler)el_end);
	XML_SetCdataSectionHandler (p, (XML_StartCdataSectionHandler)cdata_start, (XML_EndCdataSectionHandler)cdata_end);

	pd.r = NULL;

	fd = fopen (file, "r");

	if (fd == NULL) {
		odk_log_warning ("Unable to open %s", file);
		return NULL;
	}

	while (1) {
		int done;
		int len;

		len = fread (buff, 1, BUFFSIZE, fd);
		done = feof (fd);

		XML_SetUserData (p, &pd);
		if (!XML_Parse (p, buff, len, done)) {
			odk_log_error ("Parse error at line %d:\n%s",
						   XML_GetCurrentLineNumber (p),
						   XML_ErrorString (XML_GetErrorCode (p)));
		}

		if (done)
			break;
	}

	XML_ParserFree (p);
	fclose (fd);

	while (pd.r->parent) {
		pd.r = pd.r->parent;
	}

	return pd.r;
}
