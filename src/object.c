/*
odkUtils: Utility Library
        Base Object Class and Model
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/  
    
#include <odkutils.h>
    
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
    
#ifdef WIN32
#include <windows.h>
#endif

void
object_register (void *o, char *name){   	
	Object * obj = (Object *) o;
	
	if (o == NULL){
		odk_log_warning ("Tried registering NULL Object");		
		return;	
	}
    
	obj->Class_Object.className = strcopy (name);
    obj->Class_Object.refcount = 1;
    obj->Class_Object.locked = 0;
    obj->Class_Object.locktype = OBJECT_LOCK_SIMPLE;
    obj->Class_Object.destructors = odk_slist_new (0);
	obj->Class_Object.mutex = odk_mutex_new ();
}

void
object_append_destructor (void *o, FREE_FUNC delfunc){	
	Object * obj = (Object *) o;
    odk_slist_append (OBJECT (obj).destructors, delfunc);
} 

int 
object_lock (void *o){   	
	Object * obj = (Object *) o;
	
	odk_mutex_lock(obj->Class_Object.mutex);

	return 1;
}

int 
object_trylock (void *o){   	
	Object * obj = (Object *) o;
	
	return odk_mutex_trylock(obj->Class_Object.mutex);
}

void
object_unlock (void *o){	
	Object * obj = (Object *) o;	
	
	odk_mutex_unlock(obj->Class_Object.mutex);
} 

void object_set_lock_type (void *o, char l){	
	Object * obj = (Object *) o;	
	obj->Class_Object.locktype = l;
} 

char *
object_class (void *o){	
	Object * obj = (Object *) o;
	if (o == NULL)		
		return NULL;

	return obj->Class_Object.className;
}

void
object_ref (void *o){	
	Object * obj = (Object *) o;
	if (o == NULL) {		
		odk_log_warning ("Trying to ref a NULL Object");
		return;	
	}
    
	obj->Class_Object.refcount++;
}

void 
object_unref (void *o){	
	Object * obj = (Object *) o;
    FREE_FUNC del_func;
    
	obj->Class_Object.refcount--;
    
	if (obj->Class_Object.refcount == 0) {		
		odk_mutex_destroy(obj->Class_Object.mutex);
		
		odk_log_debug ("Unregistering %s\n", obj->Class_Object.className);		
		free (obj->Class_Object.className);
		
		for_slist (OBJECT (obj).destructors) {			
			del_func = (FREE_FUNC)odk_slist_data (OBJECT (obj).destructors);			
			if (del_func)				
				(*del_func) (obj);		
		}
		
		odk_slist_free (OBJECT (obj).destructors, NULL);		
		free (o);	
	}
}
