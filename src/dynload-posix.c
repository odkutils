/*
odkUtils: Utility Library
        Cross-Platform Dynamic Library Loading
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <dlfcn.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/dynload.h>

struct odkLibHandle_s{
	void *handle;
};

odkLibHandle *
odk_library_open (char *filename)
{
	odkLibHandle *ret;
	char *newname;
	
	ret = malloc(sizeof(odkLibHandle));
	newname = malloc (strlen (filename) + 8);

	sprintf(newname, "%s%s",filename, ".so");
	ret->handle = dlopen (newname, RTLD_LAZY);

	free (newname);

	if (!ret)
		printf ("%s", dlerror ());
	return ret;
}

void *
odk_library_symbol (odkLibHandle *handle, char *symbol)
{
	void *ret;
	ret = dlsym (handle->handle, symbol);

	if (!ret)
		printf ("%s\n", dlerror ());

	return ret;
}

void
odk_library_close (odkLibHandle *handle)
{
	dlclose (handle->handle);
	free(handle);
}
