#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#include <odkutils/common.h>
#include <odkutils/bitfield.h>

unsigned char bittable[9] = { 1, 2, 4, 8, 16, 32, 64, 128, 255 };

odkBitfield
odk_bitfield_new (int n)
{
	int i;
	odkBitfield b;

	i = (n / 8) + 6;
	b = (odkBitfield) malloc (sizeof (char) * i);
	memset (b, 0, sizeof (char) * i);

	((int *) b)[0] = n;
	return b;
}

odkBitfield
odk_bitfield_resize (odkBitfield b, int n)
{
	int i, c;

	i = (n / 8) + 6;
	b = (odkBitfield) realloc (b, sizeof (char) * i);
	c = (((int *) b)[0] / 8) + 4;
	memset ((void *) (b + c), 0, sizeof (char) * (i - c));

	((int *) b)[0] = n;
	return b;
}

void
odk_bitfield_set (odkBitfield b, int n)
{
	int i, c;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return;
	}

	if (((int *) b)[0] < n) {
		printf ("Bitfield is only %i wide\n", b[0]);
		return;
	}

	i = (abs (n - 1) / 8) + 4;
	c = n - ((i - 4) * 8);
	b[i] = b[i] | bittable[c - 1];
	//printf("Set bit %i, in byte %i, c =%i\n", n, i, c);
}

void
odk_bitfield_unset (odkBitfield b, int n)
{
	int i, c;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return;
	}

	if (((int *) b)[0] < n) {
		printf ("Bitfield is only %i wide\n", b[0]);
		return;
	}

	i = (abs (n - 1) / 8) + 4;
	c = n - ((i - 4) * 8);
	b[i] = b[i] ^ (bittable[c - 1]);
	//printf("Set bit %i, in byte %i, c =%i\n", n, i, c);
}

void
odk_bitfield_clear (odkBitfield b)
{
	int i, c;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return;
	}

	c = (((int *) b)[0] / 8) + 4;

	for (i = 4; i < c; i++) {
		b[i] = 0;
	}
}

int
odk_bitfield_get (odkBitfield b, int n)
{
	int i, c;
	char d;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return 0;
	}

	if (((int *) b)[0] < n) {
		printf ("Bitfield is only %i wide\n", b[0]);
		return 0;
	}

	i = (abs (n - 1) / 8) + 4;
	c = n - ((i - 4) * 8);
	d = b[i] & bittable[c - 1];

	if (d)
		return 1;

	return 0;
}

int
odk_bitfield_next_set (odkBitfield b)
{
	int i, j, c, r;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return 0;
	}

	c = (((int *) b)[0] / 8) + 5;

	for (i = 4; i < c; i++) {
		if (b[i] ^ 0x00) {
			for (j = 0; j < 8; j++) {
				if (b[i] & bittable[j]) {
					r = ((i - 4) * 8) + j + 1;

					if (r > ((int *) b)[0])
						return 0;

					return r;
				}
			}
		}
	}

	return 0;
}

int
odk_bitfield_next_unset (odkBitfield b)
{
	int i, j, c, r;

	if (b == NULL) {
		printf ("Using Null Bitfield\n");
		return 0;
	}
	//printf("Bitfield Size: %i", ((int*)b)[0]);
	c = (((int *) b)[0] / 8) + 5;

	for (i = 4; i < c; i++) {
		if (b[i] != 0xFF) {
			for (j = 0; j < 8; j++) {
				if (!(b[i] & bittable[j])) {
					r = ((i - 4) * 8) + j + 1;

					if (r > ((int *) b)[0])
						return 0;

					return r;
				}
			}
		}
	}

	return 0;
}
