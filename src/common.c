/*
odkUtils: Utility Library
        Common Defines and Functions
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>

char *
strcopy (char *string)
{
	int ilen = 0; 
	char *rets = NULL; 

	ilen = strlen (string);
	rets = (char *) malloc (ilen + 1);

	memset (rets, 0, ilen + 1);
	memcpy (rets, string, ilen);

	return rets;
}

char *
strncopy (char *string, int l)
{
	char *rets = (char *) malloc (l + 1);
	memset (rets, 0, l + 1);
	memcpy (rets, string, l);

	return rets;
}
