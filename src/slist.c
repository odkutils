/*
odkUtils: Utility Library
        Single Linked Lists
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#ifdef NOMACROS
#undef NOMACROS
#endif

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>

odkSList *
odk_slist_new (int mempool)
{
	odkSList *list;

	list = (odkSList *) malloc (sizeof (odkSList));
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;
	if (mempool == 0) {
		list->memPool = NULL;
	} else {
		list->memPool = odk_mempool_new (sizeof (odkSNode), mempool);
	}

	return list;
}

odkSList *
odk_slist_new_shared (odkMemPool * mempool)
{
	odkSList *list;

	if (mempool == NULL)
		return odk_slist_new (0);

	list = (odkSList *) malloc (sizeof (odkSList));
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;
	list->memPool = mempool;
	mempool->refCount++;

	return list;
}

void
odk_slist_free (odkSList * list, FREE_FUNC func)
{
	odkSNode *n = NULL;
	odkSNode *m = NULL;

	if (list == NULL)
		return;

	if (list->memPool) {
		if (func) {
			for_slist (list) {
				(*func) (odk_slist_data (list));
			}
		}
		odk_mempool_free (list->memPool);
	} else {
		if (odk_slist_first (list)) {
			m = list->current;
			do {
				if (func)
					(*func) (m->data);
				n = m;
				m = n->next;
				free (n);
			} while (m != NULL);
		}
	}

	free (list);
}

void odk_slist_empty (odkSList * list, FREE_FUNC func){
	odkSNode *n;
	odkSNode *m;

	if (list == NULL)
		return;

	if (list->memPool) {
		if (func) {
			for_slist (list) {
				(*func) (odk_slist_data (list));
			}
		}
		odk_mempool_free (list->memPool);
	} else {
		if (odk_slist_first (list)) {
			m = list->current;
			do {
				if (func)
					(*func) (m->data);
				n = m;
				m = n->next;
				free (n);
			} while (m != NULL);
		}
	}
	
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;
}

bool
odk_slist_append (odkSList * list, void *data)
{
	odkSNode *n;

	if (list == NULL)
		return false;

	n = (odkSNode *) odk_mempool_alloc (list->memPool, sizeof (odkSNode));
	n->next = NULL;
	n->data = data;

	if (list->first == NULL) {
		list->first = n;
		list->current = n;
		list->last = n;
	} else {
		list->last->next = n;
		list->last = n;
	}

	list->count++;
	return true;
}

bool
odk_slist_preappend (odkSList * list, void *data)
{
	odkSNode *n;

	if (list == NULL)
		return false;

	n = (odkSNode *) odk_mempool_alloc (list->memPool, sizeof (odkSNode));
	n->next = NULL;
	n->data = data;

	if (list->first == NULL) {
		list->first = n;
		list->current = n;
		list->last = n;
	} else {
		n->next = list->first;
		list->first = n;
	}

	list->count++;
	return true;
}

void *
_odk_slist_data (odkSList * list)
{
	return odk_slist_data (list);
}

bool
odk_slist_next (odkSList * list)
{
	if (list == NULL)
		return false;

	if (list->current == NULL)
		return false;

	list->current = list->current->next;

	if (list->current == NULL)
		return false;

	list->cindex++;
	return true;
}

bool
odk_slist_first (odkSList * list)
{
	if ((list == NULL) || (list->first == NULL))
		return false;
	/*if (list->first == NULL)
	   return false; */

	list->cindex = 0;
	list->current = list->first;
	return true;
}

bool
odk_slist_last (odkSList * list)
{
	if ((list == NULL) || (list->last == NULL))
		return false;
	/*if (list->last == NULL)
	   return false; */

	list->cindex = list->count;
	list->current = list->last;
	return true;
}

bool
odk_slist_delete (odkSList * list)
{
	odkSNode *n;

	if (list == NULL)
		return false;

	if (list->first == NULL)
		return false;

	if (list->current == list->first) {
		if (list->current->next == NULL) {	// The first and only element
			odk_mempool_dealloc (list->memPool, list->first);
			list->current = NULL;
			list->first = NULL;
			list->last = NULL;
			list->count = 0;
		} else {
			if (odk_slist_next (list)) {
				odk_mempool_dealloc (list->memPool, list->first);
				list->first = list->current;
				list->count--;
			} else {
				return false;
			}
		}
	} else {
		n = list->first;
		while (n->next != list->current) {
			n = n->next;
		}
		n->next = list->current->next;

		odk_mempool_dealloc (list->memPool, list->current);
		list->count--;
		list->current = n;
	}

	return true;
}

int
_odk_slist_count (odkSList * list)
{
	return odk_slist_count (list);
}

bool
_odk_slist_eof (odkSList * list)
{
	if (list == NULL)
		return true;

	if (list->current == NULL)
		return true;

	return false;
}

void *
odk_slist_find (odkSList * list, void *d, LISTFIND_FUNC func)
{
	for_slist (list) {
		if (func (odk_slist_data (list), d))
			return odk_slist_data (list);
	}

	return NULL;
}

void *
odk_slist_find_string (odkSList * list, char *d)
{
	for_slist (list) {
		if (!strcmp ((char *) odk_slist_data (list), d))
			return odk_slist_data (list);
	}

	return NULL;
}

bool odk_slist_nth(odkSList *list, int n){
	if (list == NULL)
		return false;
	
	if (list->current = NULL)
		return false;
	
	
	if (n > list->count)
		return false;
	
	if (n == list->cindex)
		return true;
	
	if (n == 0){
		odk_slist_first(list);
		return true;
	}
	
	if (n == list->count){
		odk_slist_last(list);
		return true;
	}
		
	if (n < list->cindex){
		odk_slist_first(list);
		while(list->cindex != n)
			odk_slist_next(list);
		return true;
	}
	
	while(list->cindex != n)
		odk_slist_next(list);
	
	return true;
}

void **odk_slist_toarray (odkSList * list){
	void **ret;
	int i = 0;
	
	if (list == NULL)
		return NULL;
	
	if (odk_slist_count(list) == 0)
		return NULL;
	
	ret = malloc(sizeof(void*) * odk_slist_count(list));
	
	if (ret == NULL)
		return NULL;
	
	for_slist(list){
		ret[i] = odk_slist_data(list);
		i++;
	}
	
	return ret;
}

int odk_slist_index (odkSList *list){
	if (list == NULL)
		return -1;
	
	if (odk_slist_count(list) == 0)
		return -1;
	
	return list->cindex;
}


int odk_slist_is_first (odkSList *list){
	if (list == NULL)
		return 1;
	
	if (odk_slist_count(list) == 0)
		return 1;
	
	if (list->current == list->first)
		return 1;
	
	return 0;
}

int odk_slist_is_last (odkSList *list){
	if (list == NULL)
		return 1;
	
	if (odk_slist_count(list) == 0)
		return 1;
	
	if (list->current->next == NULL)
		return 1;
	
	return 0;
}

void *odk_slist_pop(odkSList *list, int last){
	void *ret;
	
	if (list == NULL)
		return 0;
	
	if (last){
		odk_slist_first(list);
	}else{
		odk_slist_last(list);
	}
	
	ret = odk_slist_data(list);
	odk_slist_delete(list);
	
	return ret;
}

int odk_slist_push(odkSList *list, void *data, int last){
	if (list == NULL)
		return 0;
	
	if (last)
		return odk_slist_append(list, data);
	
	return odk_slist_preappend(list, data);
}
