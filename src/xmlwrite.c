/*
odkUtils: Utility Library
        XML writer
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>
#include <odkutils/hashtable.h>
#include <odkutils/xml.h>

static char *tablevel[11] =
	{ "", "\t", "\t\t", "\t\t\t", "\t\t\t\t", "\t\t\t\t\t",
	"\t\t\t\t\t\t", "\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t",
	"\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t"
};

void
node_write (odkXMLNode * n, FILE * fd, int *level)
{
	odkHashEl *el;
	odkXMLNode *c;
	int i;

	if (level) {
		if (*level < 11) {
			fprintf (fd, tablevel[*level]);
		} else {
			for (i = 0; i < *level; i++)
				fprintf (fd, "\t");
		}
	}

	fprintf (fd, "<%s", n->name);

	if (odk_slist_first (n->attribs)) {
		do {
			el = (odkHashEl *) odk_slist_data (n->attribs);
			fprintf (fd, " %s=\"%s\"", el->key, el->value);
		} while (odk_slist_next (n->attribs));
	}

	if ((odk_slist_count(n->children) == 0) && (n->data == NULL) && (!n->cdata)){
		fprintf (fd, "/>\n");
		return;
	}
	
	fprintf (fd, ">");

	if (odk_slist_count (n->children) > 0) {
		fprintf (fd, "\n");

		for_slist (n->children) {
			c = (odkXMLNode *) odk_slist_data (n->children);

			if (level)
				(*level)++;

			node_write (c, fd, level);

			if (level)
				(*level)--;
		}

		if (level) {
			if (*level < 11) {
				fprintf (fd, tablevel[*level]);
			} else {
				for (i = 0; i < *level; i++)
					fprintf (fd, "\t");
			}
		}
	} else {
		if (n->cdata)
			fprintf (fd, "<![CDATA[");

		fprintf (fd, "%s", n->data);

		if (n->cdata)
			fprintf (fd, "]]>");
	}

	fprintf (fd, "</%s>\n", n->name);
}

int
odk_xml_write (char *file, odkXMLNode * n)
{
	FILE *fd;
	int level = 0;

	fd = fopen (file, "w");
	fprintf (fd, "<?xml version=\"1.0\"?>\n");

	node_write (n, fd, &level);

	fclose (fd);

	return 1;
}
