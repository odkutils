/*
odkUtils: Utility Library
        Doubly Linked Lists
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#ifdef NOMACROS
#undef NOMACROS
#endif

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/list.h>

odkList *
odk_list_new (int mempool)
{
	odkList *list;

	list = (odkList *) malloc (sizeof (odkList));
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;

	if (mempool == 0) {
		list->memPool = NULL;
	} else {
		list->memPool = odk_mempool_new (sizeof (odkNode), mempool);
	}

	return list;
}

odkList *
odk_list_new_shared (odkMemPool * mempool)
{
	odkList *list;

	if (mempool == NULL)
		return odk_list_new (0);

	list = (odkList *) malloc (sizeof (odkList));
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;
	list->memPool = mempool;
	mempool->refCount++;

	return list;
}

bool
odk_list_delete (odkList * list)
{
	odkNode *n;

	if (list == NULL)
		return false;

	if (list->first == NULL)
		return false;

	if (list->current == list->first) {
		if (list->current->next == NULL) {	// The first and only element
			odk_mempool_dealloc (list->memPool, list->first);
			list->current = NULL;
			list->first = NULL;
			list->last = NULL;
			list->count = 0;
		} else {
			if (odk_list_next (list)) {
				odk_mempool_dealloc (list->memPool, list->first);
				list->first = list->current;
				list->count--;
				list->current->prev = NULL;
			} else {
				return false;
			}
		}
	} else if (list->current == list->last) {
		if (odk_list_prev (list)) {
			odk_mempool_dealloc (list->memPool, list->last);
			list->last = list->current;
			list->count--;
			list->current->next = NULL;
		} else {
			return false;
		}
	} else {
		n = list->current;
		list->current->prev->next = list->current->next;
		list->current->next->prev = list->current->prev;
		list->current = list->current->next;
		odk_mempool_dealloc (list->memPool, n);
	}
	
	return true;
}

void odk_list_empty (odkList * list, FREE_FUNC func){
	odkNode *n;
	odkNode *m;

	if (list == NULL)
		return;

	if (list->memPool) {
		if (func) {
			for_list (list) {
				(*func) (odk_list_data (list));
			}
		}
		odk_mempool_free (list->memPool);
	} else {
		if (odk_list_first (list)) {
			m = list->current;
			do {
				if (func)
					(*func) (m->data);
				n = m;
				m = n->next;
				free (n);
			} while (m != NULL);
		}
	}
	
	list->cindex = 0;
	list->first = NULL;
	list->last = NULL;
	list->current = NULL;
	list->count = 0;
}

bool
odk_list_insert (odkList * list, void *data)
{
	odkNode *n;

	if (list == NULL)
		return false;

	if (list->count == 0)
		return odk_list_append (list, data);

	if (list->current->next == NULL)
		return odk_list_append (list, data);

	n = (odkNode *) odk_mempool_alloc (list->memPool, sizeof (odkNode));

	n->next = NULL;
	n->prev = NULL;
	n->data = data;

	n->next = list->current->next;
	n->prev = list->current;

	list->current->next->prev = n;
	list->current->next = n;

	list->count++;
	return true;
}

bool
odk_list_append (odkList * list, void *data)
{
	odkNode *n;

	if (list == NULL)
		return false;

	n = (odkNode *) odk_mempool_alloc (list->memPool, sizeof (odkNode));
	n->next = NULL;
	n->prev = NULL;
	n->data = data;

	if (list->first == NULL) {
		list->first = n;
		list->current = n;
		list->last = n;
	} else {
		list->last->next = n;
		n->prev = list->last;
		list->last = n;
	}

	list->count++;
	return true;
}

bool
odk_list_preappend (odkList * list, void *data)
{
	odkNode *n;

	if (list == NULL)
		return false;

	n = (odkNode *) odk_mempool_alloc (list->memPool, sizeof (odkNode));
	n->next = NULL;
	n->prev = NULL;
	n->data = data;

	if (list->first == NULL) {
		list->first = n;
		list->current = n;
		list->last = n;
	} else {
		list->first->prev = n;
		n->next = list->first;
		list->first = n;
	}

	list->count++;
	return true;
}

void *
_odk_list_data (odkList * list)
{
	if ((list == NULL) || (list->current == NULL))
		return NULL;
	/*if (list->current == NULL)
	   return NULL; */

	return list->current->data;
}

bool
odk_list_next (odkList * list)
{
	if (list == NULL)
		return false;

	if (list->current == NULL)
		return false;

	list->current = list->current->next;

	if (list->current == NULL)
		return false;

	list->cindex++;
	return true;
}

bool
odk_list_prev (odkList * list)
{
	if ((list == NULL) || (list->current == NULL)
		|| (list->current->prev == NULL))
		return false;
	/*if (list->current == NULL)
	   return false;
	   if (list->current->prev == NULL)
	   return false; */

	list->current = list->current->prev;
	list->cindex--;
	return true;
}

bool
odk_list_first (odkList * list)
{
	if ((list == NULL) || (list->first == NULL))
		return false;
	/*if (list->first == NULL)
	   return false; */
	
	list->current = list->first;
	list->cindex = 0;
	return true;
}

bool
odk_list_last (odkList * list)
{
	if ((list == NULL) || (list->last == NULL))
		return false;
	/*if (list->last == NULL)
	   return false; */

	list->current = list->last;
	list->cindex = list->count;
	return true;
}

int
_odk_list_count (odkList * list)
{
	if (list == NULL)
		return 0;

	return list->count;
}

bool
_odk_list_bof (odkList * list)
{
	if (list == NULL)
		return true;
	if (list->current->prev == NULL)
		return true;

	return false;
}

bool
_odk_list_eof (odkList * list)
{
	if (list == NULL)
		return true;

	if (list->current == NULL)
		return true;

	return false;
}

void *
odk_list_find (odkList * list, void *d, LISTFIND_FUNC func)
{
	for_list (list) {
		if (func (odk_list_data (list), d))
			return odk_list_data (list);
	}

	return NULL;
}

void *
odk_list_find_string (odkList * list, char *d)
{
	for_list (list) {
		if (!strcmp ((char *) odk_list_data (list), d))
			return odk_list_data (list);
	}

	return NULL;
}

void
odk_list_free (odkList * list, FREE_FUNC func)
{
	odkNode *n;
	odkNode *m;

	if (list == NULL)
		return;

	if (list->memPool) {
		if (func) {
			for_list (list) {
				(*func) (odk_list_data (list));
			}
		}
		odk_mempool_free (list->memPool);
	} else {
		if (odk_list_first (list)) {
			m = list->current;
			do {
				if (func)
					(*func) (m->data);
				n = m;
				m = n->next;
				free (n);
			} while (m != NULL);
		}
	}

	free (list);
}

bool odk_list_nth(odkList *list, int n){
	int d1, d2;
	
	if (list == NULL)
		return false;
	
	if (list->current = NULL)
		return false;
	
	
	if (n > list->count)
		return false;
	
	if (n == list->cindex)
		return true;
	
	if (n == 0){
		odk_list_first(list);
		return true;
	}
	
	if (n == list->count){
		odk_list_last(list);
		return true;
	}
	
	if (n < list->cindex){
		d1 = list->cindex - n;
		if (n < d1){
			odk_list_first(list);
			while(list->cindex != n)
				odk_list_next(list);
		}else{
			while(list->cindex != n)
				odk_list_prev(list);
		}
		return true;
	}
	
	d1 = n - list->cindex;
	d2 = list->count - n;
	
	if (d1 > d2){
		odk_list_last(list);
		while(list->cindex != n)
			odk_list_prev(list);
	}else{
		while(list->cindex != n)
			odk_list_next(list);
	}
	
	return true;
}

void **odk_list_toarray (odkList * list){
	void **ret;
	int i = 0;
	
	if (list == NULL)
		return NULL;
	
	if (odk_list_count(list) == 0)
		return NULL;
	
	ret = malloc(sizeof(void*) * odk_list_count(list));
	
	if (ret == NULL)
		return NULL;
	
	for_list(list){
		ret[i] = odk_list_data(list);
		i++;
	}
	
	return ret;
}

int odk_list_index (odkList *list){
	if (list == NULL)
		return -1;
	
	if (odk_list_count(list) == 0)
		return -1;
	
	return list->cindex;
}

int odk_list_is_first (odkList *list){
	if (list == NULL)
		return 1;
	
	if (odk_list_count(list) == 0)
		return 1;
	
	if (list->current->prev == NULL)
		return 1;
	
	return 0;
}

int odk_list_is_last (odkList *list){
	if (list == NULL)
		return 1;
	
	if (odk_list_count(list) == 0)
		return 1;
	
	if (list->current->next == NULL)
		return 1;
	
	return 0;
}

void *odk_list_pop(odkList *list, int last){
	void *ret;
	
	if (list == NULL)
		return 0;
	
	if (last){
		odk_list_first(list);
	}else{
		odk_list_last(list);
	}
	
	ret = odk_list_data(list);
	odk_list_delete(list);
	
	return ret;
}

int odk_list_push(odkList *list, void *data, int last){
	if (list == NULL)
		return 0;
	
	if (last)
		return odk_list_append(list, data);
	
	return odk_list_preappend(list, data);
}

odkNode *odk_list_node(odkList *list){
	return list->current;
}

int odk_list_swap(odkList *list, odkNode *n1, odkNode *n2){
	void *d;
	
	d = n2->data;
	n2->data = n1->data;
	n1->data = d;
	
	return 0;
}

int odk_list_sort_partition(odkList *list, odkNode *s, odkNode *e, CMP_FUNC ocmp){
	int pindex;
	odkNode *pivot, *el1, *el2, *el3;
	
	if (s->next == e){
		if (ocmp(s->data, e->data) > 0){
			odk_list_swap(list, s, e);
		}
		return 0;
	}
	//TODO: Choose a better pivot.
	pivot = e;
	
	el1 = s;
	
	while(1){
		el3 = el1->next;
		if (ocmp(el1->data, pivot->data) > 0){
			el2 = el1->next;
			
			while((ocmp(el1->data, el2->data) < 1) && (el2 != pivot)){
				el2 = el2->next;
			}
			
			odk_list_swap(list, el1, el2);
			
			//Pivot is in the final place
			if (el2 == pivot){
				pivot = el1;
				if ((pivot->next != list->last) && (pivot->next != e)){
					odk_list_sort_partition(list, pivot->next, el2, ocmp);
				}
				if ((pivot->prev != list->first) && (pivot->prev != s)){
					odk_list_sort_partition(list, list->first, pivot->prev, ocmp);
				}
				return 0;
			}
			el3 = el1;
		}
		
		if ((el3 == e) || (el3 == NULL)){
			return 0;
		}
		
		el1 = el3;
	}
}

odkList *odk_list_new_fromarray(void **array, int n){
	int i;
	odkList *list;
	
	list = odk_list_new(0);
	for (i = 0; i < n; i++){
		odk_list_append(list, array[i]);
	}
	
	return list;
}

int odk_list_quicksort(odkList *list, CMP_FUNC ocmp){
	odk_list_sort_partition(list, list->first, list->last, ocmp);
}
