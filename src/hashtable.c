
/*
 * odkUtils: Utility Library Generic Hash Table
 * 
 * Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela Contact me:
 * <clsdaniel@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>
#include <odkutils/hashtable.h>

static int
hash_gen (const char *key)
{
	unsigned int hash_num = 5381;
	const unsigned char *ptr;

	if (!key)
		return 0;
	for (ptr = (unsigned char *) key; *ptr; ptr++)
		hash_num = (hash_num * 33) ^ *ptr;

	hash_num &= 0xff;
	return (int) hash_num;
}

odkHashTable *
odk_hashtable_new (int mempool)
{
	int i;
	odkHashTable *ht = (odkHashTable *) malloc (sizeof (odkHashTable));

	ht->count = 0;

	for (i = 0; i < 255; i++) {
		ht->buckets[i] = NULL;
	}
	return ht;
}

static void *
lookup_digest (odkHashTable * ht, int k, char *key)
{
	odkHashEl *el;
	odkSList *l = ht->buckets[k];

	if (l == NULL)
		return NULL;
	
	for_slist(l){
		el = odk_slist_data (l);
		if (el) {
			if (!strcmp (el->key, key))
				return el->value;
		}
	}

	return NULL;
}

int
odk_hashtable_insert (odkHashTable * ht, char *key, void *value)
{
	int k;
	odkHashEl *el = (odkHashEl *) odk_mempool_alloc (NULL, sizeof (odkHashEl));

	el->key = strcopy(key);
	el->value = value;

	if (ht == NULL)
		return 0;

	k = hash_gen (key);

	if (ht->buckets[k] == NULL) {
		ht->buckets[k] = odk_slist_new (0);
		odk_slist_append (ht->buckets[k], el);
	} else {
		if (lookup_digest (ht, k, key)) {
			free (el);
			return 0;
		}

		odk_slist_append (ht->buckets[k], el);
	}

	ht->count++;
	return 1;
}

void *
odk_hashtable_lookup (odkHashTable * ht, char *key)
{
	int k;

	if (ht == NULL)
		return 0;

	k = hash_gen (key);
	return lookup_digest (ht, k, key);
}

void
hashel_free (odkHashEl * el)
{
	free (el->key);
	free (el);
}

void
odk_hashtable_free (odkHashTable * ht, FREE_FUNC * free_func)
{
	int i;
	odkSList *l;
	odkHashEl *el;

	if (free_func) {
		for (i = 0; i < 256; i++) {
			l = ht->buckets[i];
			if (l != NULL) {
				for_slist (l) {
					el = odk_slist_data (l);
					(*free_func) (el->value);
				}
				odk_slist_free (l, (FREE_FUNC)&hashel_free);
			}
		}
	} else {
		for (i = 0; i < 256; i++) {
			l = ht->buckets[i];
			if (l != NULL)
				odk_slist_free (l, (FREE_FUNC) & hashel_free);

		}
	}
}
