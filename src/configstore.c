/*
odkUtils: Utility Library
        Configuration Storing Facility
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

#include <odkutils.h>

char *
odk_config_key_get (odkConfigStore * cs, char *key)
{
	if (cs == NULL)
		return NULL;

	if (key == NULL)
		return NULL;

	return cs->Class_ConfigStore.get_key (cs, key);
}

int
odk_config_key_set (odkConfigStore * cs, char *key, char *value)
{
	if (cs == NULL)
		return false;

	if (key == NULL)
		return false;

	cs->Class_ConfigStore.set_key (cs, key, value);
	return true;
}

int
odk_config_key_geti (odkConfigStore * cs, char *key)
{
	if (cs == NULL)
		return 0;

	if (key == NULL)
		return 0;

	return cs->Class_ConfigStore.get_keyi (cs, key);
}

int
odk_config_key_seti (odkConfigStore * cs, char *key, int value)
{
	if (cs == NULL)
		return false;

	if (key == NULL)
		return false;

	cs->Class_ConfigStore.set_keyi (cs, key, value);
	return true;
}

float
odk_config_key_getf (odkConfigStore * cs, char *key)
{
	if (cs == NULL)
		return 0.0;

	if (key == NULL)
		return 0.0;

	return cs->Class_ConfigStore.get_keyf (cs, key);
}

int
odk_config_key_setf (odkConfigStore * cs, char *key, float value)
{
	if (cs == NULL)
		return false;

	if (key == NULL)
		return false;

	cs->Class_ConfigStore.set_keyf (cs, key, value);
	return true;
}

odkSList *
odk_config_key_list (odkConfigStore * cs, char *path)
{
	return cs->Class_ConfigStore.get_keylist (cs, path);
}

int
odk_config_key_type (odkConfigStore * cs, char *key)
{
	return cs->Class_ConfigStore.get_keytype (cs, key);
}

int
odk_config_save (odkConfigStore * cs)
{
	if (cs == NULL)
		return false;

	return cs->Class_ConfigStore.save (cs);
}

int
odk_config_load (odkConfigStore * cs)
{
	if (cs == NULL)
		return false;

	return cs->Class_ConfigStore.load (cs);
}
