/*
odkUtils: Utility Library
        XML Configuration Backend
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>
#include <odkutils_xml.h>

char g_buff[50];

int
odk_xmlconfig_path_set (odkXMLNode * n, char *path, char *value, char *type)
{
	int l;
	char *ptr, *ptrn;
	char buff[100];
	odkXMLNode *p;

	if (n == NULL)
		return false;

	if (path == NULL)
		return false;

	if (path[0] != '/')
		return false;

	if (strlen(path) == 1) {
		odk_xml_data_set (n, value);
		return true;
	}

	p = n;
	ptr = path + 1;
	memset(buff, 0, 100);

	while((ptrn = strchr(ptr, '/')) != NULL){
		l = (int) (ptrn - ptr);
		memcpy (buff, ptr, l);

		n = odk_xml_child_get (p, buff);
		if (n == NULL){
			n = odk_xml_node_new (p, buff);
			odk_xml_attrib_set (n, "type", "dir");
		}

		p = n;
		ptr = ptrn + 1;
	}
	/*while ((ptrn = strpbrk (ptr, "/"))) {
		l = (int) (ptrn - ptr);
		memcpy (buff, ptr, l);
		buff[l] = 0;

		//odk_log_debug (buff);

		n = odk_xml_child_get (p, buff);
		if (n == NULL)
			n = odk_xml_node_new (p, buff);

		odk_xml_attrib_set (n, "type", "dir");

		p = n;
		ptr = ptrn + 1;
	}*/

	l = strlen (path) - (int) (ptr - path);

	memcpy (buff, ptr, l);
	buff[l] = 0;
	//odk_log_debug (buff);
	n = odk_xml_child_get (p, buff);

	if (n == NULL)
		n = odk_xml_node_new (p, buff);

	odk_xml_data_set (n, value);
	odk_xml_attrib_set (n, "type", type);
	return true;
}

char *
odk_xmlconfig_key_get (odkXMLConfig * cfg, char *key)
{
	return odk_xml_path_data_get (cfg->root, key);
}

void
odk_xmlconfig_key_set (odkXMLConfig * cfg, char *key, char *val)
{
	if (cfg->root == NULL) {
		if (cfg->rootname == NULL) {
			cfg->root = odk_xml_node_new (NULL, "config");
		} else {
			cfg->root = odk_xml_node_new (NULL, cfg->rootname);
		}
		odk_xml_attrib_set (cfg->root, "type", "dir");
	}

	if (val == NULL) {
		odk_xmlconfig_path_set (cfg->root, key, NULL, "dir");
	} else {
		odk_xmlconfig_path_set (cfg->root, key, val, "text");
	}
}

int
odk_xmlconfig_key_geti (odkXMLConfig * cfg, char *key)
{
	char *r;
	r = odk_xml_path_data_get (cfg->root, key);

	if (r == NULL)
		return 0;

	return atoi (r);
}

int
odk_xmlconfig_key_seti (odkXMLConfig * cfg, char *key, int val)
{
	if (cfg->root == NULL) {
		if (cfg->rootname == NULL) {
			cfg->root = odk_xml_node_new (NULL, "config");
		} else {
			cfg->root = odk_xml_node_new (NULL, cfg->rootname);
		}
		odk_xml_attrib_set (cfg->root, "type", "dir");
	}

	sprintf (g_buff, "%i", val);
	odk_xmlconfig_path_set (cfg->root, key, g_buff, "integer");
	return 1;
}

float
odk_xmlconfig_key_getf (odkXMLConfig * cfg, char *key)
{
	char *r;
	r = odk_xml_path_data_get (cfg->root, key);

	if (r == NULL)
		return 0;

	return (float) atof (r);
}

int
odk_xmlconfig_key_setf (odkXMLConfig * cfg, char *key, float val)
{
	if (cfg->root == NULL) {
		if (cfg->rootname == NULL) {
			cfg->root = odk_xml_node_new (NULL, "config");
		} else {
			cfg->root = odk_xml_node_new (NULL, cfg->rootname);
		}
		odk_xml_attrib_set (cfg->root, "type", "dir");
	}

	sprintf (g_buff, "%f", val);
	odk_xmlconfig_path_set (cfg->root, key, g_buff, "float");

	return 1;
}

odkSList *
odk_xmlconfig_key_list (odkXMLConfig * cfg, char *path)
{
	odkXMLNode *n, *t;
	odkSList *ret;

	n = odk_xml_path (cfg->root, path);

	if (n == NULL)
		return NULL;

	ret = odk_slist_new (0);

	for_slist (n->children) {
		t = odk_slist_data (n->children);

		odk_slist_append (ret, strcopy (odk_xml_name (t)));
	}

	return ret;
}

int
odk_xmlconfig_key_type (odkXMLConfig * cfg, char *key)
{
	odkXMLNode *n;
	char *t;
	int ret;

	n = odk_xml_path (cfg->root, key);

	if (n == NULL)
		return -1;

	t = odk_xml_attrib_get (n, "type");

	switch (t[0]) {
	case 't':
		ret = KEY_TEXT;
		break;
	case 'i':
		ret = KEY_INTEGER;
		break;
	case 'f':
		ret = KEY_FLOAT;
		break;
	case 'd':
		ret = KEY_DIR;
		break;
	default:
		ret = -1;
		break;
	}

	return ret;
}

int
odk_xmlconfig_save (odkXMLConfig * cfg)
{
	if (cfg->cnum == 1)
		return true;

	if (cfg->filename == NULL)
		return false;

	return odk_xml_write (cfg->filename, cfg->root);
}

int
odk_xmlconfig_load (odkXMLConfig * cfg)
{
	if (cfg->cnum == 1)
		return true;

	if (cfg->filename == NULL)
		return false;

	cfg->root = odk_xml_parse (cfg->filename);

	if (cfg->root == NULL)
		return false;

	return true;
}

void
odk_xmlconfig_delete (odkXMLConfig * cfg)
{
	if (cfg->filename)
		free (cfg->filename);

	if (cfg->rootname)
		free (cfg->rootname);

	if (cfg->cnum == 1)
		return;

	odk_xml_free (cfg->root);
}

odkXMLConfig *
odk_xmlconfig_new1 (odkXMLNode * n)
{
	odkXMLConfig *cfg = (odkXMLConfig *) malloc (sizeof (odkXMLConfig));
	object_register (cfg, "odkXMLConfig");
	cfg->filename = NULL;
	cfg->rootname = NULL;

	cfg->Class_ConfigStore.get_key =
		(FUNC_CONF_GETKEY ) & odk_xmlconfig_key_get;
	cfg->Class_ConfigStore.set_key =
		(FUNC_CONF_SETKEY ) & odk_xmlconfig_key_set;
	cfg->Class_ConfigStore.get_keyi =
		(FUNC_CONF_GETKEYI ) & odk_xmlconfig_key_geti;
	cfg->Class_ConfigStore.set_keyi =
		(FUNC_CONF_SETKEYI ) & odk_xmlconfig_key_seti;
	cfg->Class_ConfigStore.get_keyf =
		(FUNC_CONF_GETKEYF ) & odk_xmlconfig_key_getf;
	cfg->Class_ConfigStore.set_keyf =
		(FUNC_CONF_SETKEYF ) & odk_xmlconfig_key_setf;
	cfg->Class_ConfigStore.get_keylist =
		(FUNC_CONF_KEYLIST ) & odk_xmlconfig_key_list;
	cfg->Class_ConfigStore.get_keytype =
		(FUNC_CONF_KEYTYPE ) & odk_xmlconfig_key_type;
	cfg->Class_ConfigStore.save = (FUNC_CONF_SAVE ) & odk_xmlconfig_save;
	cfg->Class_ConfigStore.load = (FUNC_CONF_LOAD ) & odk_xmlconfig_load;
	cfg->root = n;
	cfg->cnum = 1;

	return cfg;
}

odkXMLConfig *
odk_xmlconfig_new (char *file, char *rootname)
{
	odkXMLConfig *cfg = (odkXMLConfig *) malloc (sizeof (odkXMLConfig));

	object_register (cfg, "odkXMLConfig");
	object_append_destructor (cfg, (FREE_FUNC)&odk_xmlconfig_delete);
	cfg->filename = strcopy (file);
	cfg->rootname = strcopy (rootname);

	cfg->Class_ConfigStore.get_key =
		(FUNC_CONF_GETKEY ) & odk_xmlconfig_key_get;
	cfg->Class_ConfigStore.set_key =
		(FUNC_CONF_SETKEY ) & odk_xmlconfig_key_set;
	cfg->Class_ConfigStore.get_keyi =
		(FUNC_CONF_GETKEYI ) & odk_xmlconfig_key_geti;
	cfg->Class_ConfigStore.set_keyi =
		(FUNC_CONF_SETKEYI ) & odk_xmlconfig_key_seti;
	cfg->Class_ConfigStore.get_keyf =
		(FUNC_CONF_GETKEYF ) & odk_xmlconfig_key_getf;
	cfg->Class_ConfigStore.set_keyf =
		(FUNC_CONF_SETKEYF ) & odk_xmlconfig_key_setf;
	cfg->Class_ConfigStore.get_keylist =
		(FUNC_CONF_KEYLIST ) & odk_xmlconfig_key_list;
	cfg->Class_ConfigStore.get_keytype =
		(FUNC_CONF_KEYTYPE ) & odk_xmlconfig_key_type;
	cfg->Class_ConfigStore.save = (FUNC_CONF_SAVE ) & odk_xmlconfig_save;
	cfg->Class_ConfigStore.load = (FUNC_CONF_LOAD ) & odk_xmlconfig_load;
	cfg->root = NULL;
	cfg->cnum = 0;

	return cfg;
}
