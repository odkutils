/*
odkUtils: Utility Library
        Memory Pools
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/logs.h>
#include <odkutils/mempool.h>

odkMemPool *
odk_mempool_new (int objSize, int maxCount)
{
	odkMemPool *pool;

	pool = (odkMemPool *) malloc (sizeof (odkMemPool));
	pool->objSize = objSize;
	pool->maxCount = maxCount;
	pool->objCount = 0;
	pool->next = NULL;
	pool->memPool = NULL;
	pool->refCount = 1;
	return pool;
}

void
odk_mempool_free (odkMemPool * pool)
{
	odkMemPool *p;

	if (pool == NULL) {
		odk_log_debug ("No pool to free");
		return;
	}

	pool->refCount--;

	if (pool->refCount > 0)
		return;

	if (pool->memPool == NULL) {
		odk_log_debug ("No memory pool to free");
		free (pool);
		return;
	}

	p = pool;

	do {
		//printf("Freeing pool memory\n");
		free (p->memPool);
		p = p->next;
	} while (p != NULL);

	do {
		//printf("Freeing memory pool lists\n");
		p = pool;
		pool = pool->next;

		free (p);
	} while (pool != NULL);
}

void *
odk_mempool_alloc (odkMemPool * pool, int objSize)
{
	void *raddr;

	if (pool == NULL) {
		return malloc (objSize);
	}

	if (pool->memPool == NULL) {
		pool->objCount = 0;
		pool->memPool = malloc (pool->objSize * pool->maxCount);
	}

	while (pool->next != NULL) {
		pool = pool->next;
	}

	if (pool->objCount == pool->maxCount) {
		pool->next = (odkMemPool *) malloc (sizeof (odkMemPool));
		pool->next->objSize = pool->objSize;
		pool->next->maxCount = pool->maxCount;

		pool = pool->next;

		pool->objCount = 0;
		pool->memPool = malloc (pool->objSize * pool->maxCount);
		pool->next = NULL;
	}

	if (pool->memPool == NULL) {
		odk_log_error ("Unable to alloc mempool of size (%i * %i)",
					   pool->objSize, pool->maxCount);
	}

	raddr = (void *) ((int) pool->memPool + (pool->objCount * pool->objSize));
	pool->objCount++;

	return raddr;
}

void
odk_mempool_dealloc (odkMemPool * pool, void *ptr)
{
	if (pool == NULL) {
		free (ptr);
		return;
	}
}
