/*
odkUtils: Utility Library
        Structures to help manipulate XML
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils/common.h>
#include <odkutils/mempool.h>
#include <odkutils/slist.h>
#include <odkutils/hashtable.h>
#include <odkutils/xml.h>

int
attrib_find (odkHashEl * e1, char *key)
{
	return !strcmp (e1->key, key);
}

void
attrib_free (odkHashEl * e1)
{
	free (e1->key);
	if (e1->value)
		free (e1->value);
	free (e1);
}

int
node_find (odkXMLNode * n, char *name)
{
	return !strcmp (n->name, name);
}

char *xml_escape(char *str){
	int l, t, i, j;
	char *ret;
	
	l = strlen(str);
	
	for (i = 0; i < l; i++){
		switch (str[i]){
			case '<':
			case '>':
				t+=4;
				break;
			case '\'':
				t+=6;
				break;
			case '&':
				t+=5;
				break;
			case 36: /* " */
			case 92: /* \ */
				t+=2;
				break;
			default:
				t++;
				break;
		}
	}
	
	t++;
	ret = malloc(sizeof(t) * sizeof(char));
	
	j = 0;
	for (i = 0; i < l; i++){
		switch (str[i]){
			case '<':
				ret[j] = '&';
				j++;
				ret[j] = 'l';
				j++;
				ret[j] = 't';
				j++;
				ret[j] = ';';
				break;
			case '>':
				ret[j] = '&';
				j++;
				ret[j] = 'g';
				j++;
				ret[j] = 't';
				j++;
				ret[j] = ';';
				break;
			case '\'':
				ret[j] = '&';
				j++;
				ret[j] = 'a';
				j++;
				ret[j] = 'p';
				j++;
				ret[j] = 'o';
				j++;
				ret[j] = 's';
				j++;
				ret[j] = ';';
				break;
			case '&':
				ret[j] = '&';
				j++;
				ret[j] = 'a';
				j++;
				ret[j] = 'm';
				j++;
				ret[j] = 'p';
				j++;
				ret[j] = ';';
				break;
			case 36:
				ret[j] = '\\';
				j++;
				ret[j] = '"';
				break;
			case 92:
				ret[j] = '\\';
				j++;
				ret[j] = '\\';
				break;
			default:
				ret[j] = str[i];
				break;
		}
		j++;
	}
	
	ret[j] = 0;
	return ret;
}

void
odk_xml_free (odkXMLNode * n)
{
	if (n == NULL)
		return;

	odk_slist_free (n->attribs, (FREE_FUNC) & attrib_free);
	odk_slist_free (n->children, (FREE_FUNC) & odk_xml_free);

	free (n->name);
	if (n->data)
		free (n->data);

	free (n);
}

odkXMLNode *
odk_xml_node_new (odkXMLNode * parent, char *name)
{
	odkXMLNode *n = (odkXMLNode *) malloc (sizeof (odkXMLNode));

	n->name = strcopy (name);
	n->data = NULL;
	n->cdata = 0;
	n->attribs = odk_slist_new (0);
	n->children = odk_slist_new (0);
	n->parent = parent;

	if (parent != NULL)
		odk_slist_append (parent->children, n);

	return n;
}

char *
odk_xml_attrib_get (odkXMLNode * n, char *attrib)
{
	odkHashEl *el;

	if (n == NULL)
		return NULL;

	el = odk_slist_find (n->attribs, attrib, (LISTFIND_FUNC) & attrib_find);

	if (el == NULL)
		return NULL;

	return el->value;
}

void
odk_xml_attrib_set (odkXMLNode * n, char *attrib, char *value)
{
	odkHashEl *el;

	if (n == NULL)
		return;

	el = odk_slist_find (n->attribs, attrib, (LISTFIND_FUNC ) &attrib_find);

	if (el == NULL) {
		el = (odkHashEl *) malloc (sizeof (odkHashEl));
		el->key = strcopy (attrib);
		el->value = strcopy (value);
		odk_slist_append (n->attribs, el);
	} else {
		free (el->value);
		el->value = strcopy (value);
	}
}

char *
odk_xml_data_get (odkXMLNode * n)
{
	if (n == NULL)
		return NULL;

	return n->data;
}

void
odk_xml_data_set (odkXMLNode * n, char *data)
{
	if (n == NULL)
		return;

	if (n->data != NULL)
		free (n->data);

	n->data = NULL;

	if (data == NULL)
		return;

	n->data = strcopy (data);
}

int odk_xml_has_children(odkXMLNode *n){
	if (n == NULL)
		return 0;
	
	if (n->children == NULL)
		return 0;
	
	if (odk_slist_count(n->children))
		return 1;
	return 0;
}

odkSList *
odk_xml_children (odkXMLNode * n)
{
	if (n == NULL)
		return NULL;

	return n->children;
}

odkXMLNode *
odk_xml_root (odkXMLNode * n)
{
	if (n == NULL)
		return NULL;

	while (n->parent != NULL) {
		n = n->parent;
	}

	return n;
}

char *
odk_xml_name (odkXMLNode * n)
{
	return n->name;
}

odkXMLNode *
odk_xml_child_get (odkXMLNode * n, char *childname)
{
	odkXMLNode *nd;

	if (n == NULL)
		return NULL;

	if (childname == NULL)
		return NULL;

	nd = odk_slist_find (n->children, childname, (LISTFIND_FUNC ) & node_find);

	return nd;
}

void
odk_xml_reparent (odkXMLNode * parent, odkXMLNode * child)
{
	if ((child == NULL) || (parent == NULL))
		return;

	child->parent = parent;

	odk_slist_append (parent->children, child);
}

odkXMLNode *
odk_xml_list_node (odkSList * nodes, char *name)
{
	odkXMLNode *nd;

	if (name == NULL) {
		odk_slist_first (nodes);
		nd = odk_slist_data (nodes);
	} else {
		nd = odk_slist_find (nodes, name, (LISTFIND_FUNC ) & node_find);
	}

	return nd;
}

int
odk_xml_path_data_set (odkXMLNode * n, char *path, char *value)
{
	int l;
	char *ptr, *ptrn;
	char buff[100];
	odkXMLNode *p;

	if (n == NULL)
		return false;

	if (path == NULL)
		return false;

	if (path[0] != '/')
		return false;

	if (strlen(path) == 1) {
		odk_xml_data_set (n, value);
		return true;
	}

	p = n;
	ptr = path + 1;

	while ((ptrn = strpbrk (ptr, "/"))) {
		l = (int) (ptrn - ptr);
		memcpy (buff, ptr, l);
		buff[l] = 0;

		//odk_log_debug (buff);

		n = odk_xml_child_get (p, buff);
		if (n == NULL)
			n = odk_xml_node_new (p, buff);

		p = n;
		ptr = ptrn + 1;
	}

	l = strlen (path) - (int) (ptr - path);

	memcpy (buff, ptr, l);
	buff[l] = 0;
	//odk_log_debug (buff);
	n = odk_xml_child_get (p, buff);

	if (n == NULL)
		n = odk_xml_node_new (p, buff);

	odk_xml_data_set (n, value);
	return true;
}

odkXMLNode *
odk_xml_path (odkXMLNode * n, char *path)
{
	int l;
	char *ptr, *ptrn;
	char buff[100];

	if (n == NULL)
		return NULL;

	if (path == NULL)
		return NULL;

	if (path[0] != '/')
		return NULL;

	l = strlen (path);

	if (l == 1)
		return n;

	ptr = path + 1;

	while ((ptrn = strpbrk (ptr, "/"))) {
		l = (int) (ptrn - ptr);
		memcpy (buff, ptr, l);
		buff[l] = 0;

		//odk_log_debug (buff);

		n = odk_xml_child_get (n, buff);

		if (n == NULL)
			return NULL;

		ptr = ptrn + 1;
	}

	l = strlen (path) - (int) (ptr - path);

	memcpy (buff, ptr, l);
	buff[l] = 0;
	//odk_log_debug (buff);
	n = odk_xml_child_get (n, buff);

	return n;
}

int odk_xml_delete(odkXMLNode *n, odkXMLNode *d){
	odkSList *child;
	odkXMLNode *t;
	
	child = odk_xml_children(n);
	
	for SLIST_ITERATOR(t, child, odkXMLNode*){
		if (t == d){
			odk_slist_delete(child);
			return 0;
		}
	}
	
	return 1;
}
