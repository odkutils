\documentclass[letterpaper,11pt]{article}
\usepackage{palatino}

\begin{document}

\begin{titlepage}
\begin{center}

\Huge{odkUtils Tutorials}

\bigskip

\large{\today}
\end{center}

\vfill

Copyright (c) 2007 by Carlos Daniel Ruvalcaba Valenzuela. 
This material may be distributed only subject to the terms 
and conditions set forth in the Open Publication License, v1.0 
or later (the latest version is presently available at 
http://www.opencontent.org/openpub/).

Distribution of the work or derivative of the work in any standard 
(paper) book form is prohibited unless prior permission is obtained 
from the copyright holder.
\end{titlepage}
\newpage

\tableofcontents
\newpage

\section{Lists}

odkUtils provides a powerful and efficent list structure with
support for both doubly linked and singly linked lists.

Each kind of list uses a different API, but they are very similar,
most of them only vary on the name, for example, odk\_slist\_new creates
a new singly linked list, while odk\_list\_new creates a doubly linked one.
The differ only in the middle word that identifies for which kind of
list the code is.

\subsection{Creating a list}

To create a basic list you must call the odk\_slist\_new or odk\_list\_new
functions:

\begin{verbatim}
odkList *list;

list = odk_list_new(0);
\end{verbatim}

The only paremeter that the list constructor function accepts is for
optimization using memory pools, look at it in later sections, for now
the value 0 is fine and wont have any impact in our code.

Now that we have a new list we can start by adding objects to it, lets
suppose that we are capturing strings, using the \emph{str} pointer, each
string pointer is dynamically allocated (via malloc for example) and thus
will need to be freed later on.

\begin{verbatim}
odk_list_append(list, str);
\end{verbatim}

The function append adds an element to the end of the list, there is also
a preappend function which adds an element to the beginning of the list.

\subsection{Iterating a list}

Lists have special functions to move the list cursor, namely odk\_list\_next,
odk\_list\_prev, odk\_list\_first, odk\_list\_last, they will return 1 if
the operation succeeded, for next it will return 0 on EOL (End of List) and
for prev it will be 0 on BOL (Beginning of List). Finally we may get the
current node data using the function odk\_list\_data, which will return
a pointer to the object stored.

\begin{verbatim}

if (odk_list_first(list)){
    do{
        str = (char*)odk_list_data(list);
        printf("Current string: %s\n", str);
    }while(odk_list_next(list));
}
\end{verbatim}

This piece of code will print each string stored on the list, you first
move the list cursor to the first element and check that the list is not
empty, then get the current node data and typecast it as needed (in 
this case as string), finally move to the next element while 
odk\_list\_next returns 1 (True).

odkUtils also includes a special macro for iterating lists, which makes it
easier as it handles the details for you, in the past example we have to
check if the list is empty, the macro does this for you an many things more.

\begin{verbatim}
char *str;

for LIST_ITERATOR(str, list, char*){
    printf("Current string: %s\n", str);
}
\end{verbatim}

As you can see, the macro takes 3 parameters, the destination of the data,
the list and the typecast for the data. The iterator macro will only iterate
the list from first element to last (forward).

In the case of singly linked list there isn't a prev function, as nodes dosen't
have a pointer to the previous element.

\subsection{Freeing a list}

Once you are done with the list you can delete it with a simple function, and
you may optionally include a function that will free each element data, taking
in count our example of a list of strings (allocated using malloc), we can free
it safely with the following code:

\begin{verbatim}
odk_list_free(list, &free);
\end{verbatim}

And that is all, no memory leaks!, you can also quickly empty a list, freeing the
elements but conserving the list:

\begin{verbatim}
odk_list_empty(list, &free);
\end{verbatim}

After this function list will be empty and ready to use again.

\subsection{List elements}

odkUtils also includes additional functions for managing list elements, for example
inserting or removing elements.

\begin{verbatim}
odk_list_insert(list, str);
\end{verbatim}

This function will insert a new element right after the current element.

\begin{verbatim}
odk_list_delete(list);
\end{verbatim}

This function will remove the current element, it will not free the element data,
you should make sure to free it before deleting it, otherwise this can cause memory
leaks.

\subsection{Queues and stacks}

The odkUtils list API includes a pair of functions to work with lists as either
queues or stacks, these are the push and pop operations, they may work in two
modes, once is to push/pop from the beginning of the list and the other from
the last element.

An example of a stack using lists, both push and pop have the same value for
the second argument, be it true (1) or false (0):

\begin{verbatim}
char *str;
odkList *list;

list = odk_list_new(0);

//Push a element to stack
odk_list_push(list, 1);

//Pop an element from the stack
str = odk_list_pop(list, 1);
\end{verbatim}

An example of queue using list, where push and pop have different values for
the second argument:

\begin{verbatim}
char *str;
odkList *list;

list = odk_list_new(0);

//Push a element to queue
odk_list_push(list, 1);

//Pop an element from the queue
str = odk_list_pop(list, 0);
\end{verbatim}

\subsection{Optimizations and other thoughts}

Lists in odkUtils are very balanced, they are very fast, safe to use
and convenient to use.

One thing you will note is that everything is done with the functions,
this may slow down the list speed as it spends extra time in calling
the function and returning, however this gives us extra safety, as
each function will check the parameters to ensure correct operation,
additionally this is necessary because odkUtils list structure is not
a node, is a structure that stores a pointer to the current element,
the first element and the last element.

The advantage of such approach is that this help us make most common functions
O(1) in complexity, appending and preappending can be done in a constant
time, moving to first and last element too.

The main disadvantage of lists is memory fragmentation, two contiguous elements
of the list may even be in different memory pages, this impacts a lot the
performance; to minimize this effect odkUtils has the additional (and optional)
optimization of using memory pools, which is a big chunk of contiguous memory
that we use to allocate elements on it, ensuring a contiguous memory layout, this
gives a big speedup on iterations and allocation/deallocation of elements.
However memory pools don't come for free as a memory pool may be a big
chunk of memory that may or may not be in complete use (may waste memory),
thus for optimal performance you need to know a good approximate of the
amount of elements that the list will contain.

Memory pools however doesn't restrict the size of the list, if you initialize
a list with a memory pool of 500 elements and you end inserting 800 (appending), the 
memory pool will have allocated two chunks of 500 elements, thus ensuring
the maximum address space contiguity possible.

One thing that may affect performance of memory pool is the order of insertion,
for example preappending is not optimum as the list in memory may be of
500 elements with the last element (in memory) being the first on the list, this
may impact performance when moving between the first and the second element, or
jumping to the first element.

In short, memory pools are a great optimization if your list will be filled
in forward order (appending) and you will just iterate it, removing elements
will really not affect performance in any noticeable way, but you may end
fragmenting the address space a bit.

To use memory pools just tell the list constructor how many elements to
preallocate, in this example we ask for 500:

\begin{verbatim}
odkList *list;

list = odk_list_new(500);
\end{verbatim}

After this you don't have to do anything else special to get the benefits of
memory pools, if you create a list with a memory pool of 0 it will not use
memory pools at all and won't waste any extra space.

\section{Regular Expressions}

As of version 0.5.8 of odkUtils it includes a new regular expression API using
the pcre engine, this API is a very easy and fast way to use regular expressions.

You should be aware that this is not a tutorial about regular expressions, but how
to use the odkUtils API to create regex objects and search a string for patterns.

\subsection{Creating and using regular expressions}

To start with regular expressions we need to create a regex object, this
is done with the odk\_regex\_new function, which takes tree parameters, one
is the pattern string, the second is the regex flags, the last one is the amount
of submatches we gonna need, if we set it to 0 or something less than 6 odkUtils will 
automatically allocate enough memory for 5 submatches, you may allocate as many as
you need.

\begin{verbatim}
odkRegex *re;

re = odk_regex_new("(http|ftp)://([./]+)", 0, 0);
\end{verbatim}

We now have a regex object which parses an URL, gets the protocol and the path, this
is very simple and it restricts it to only ftp and http protocols, we can however
do input validation with it, suppose that we have the string url = "http://odkit.sourceforge.net",
we want to see if it is a valid input:

\begin{verbatim}
if (odk_regex_validate(re, url)){
    printf("Valid URL: %s\n", url);
}else{
    printf("Erro Invalid URL!: %s\n", url);
}
\end{verbatim}

For the input we gave it, it will surely pass, however it may reject something like:
"fish://myuser\@machine.myhost.com", as it will not pass the pattern tests.

Returning with the past URL example, lets say we want to extract the protocol and
path information:


\begin{verbatim}
char proto[5];
char path[200];
odkRegex *re;
odkRegMatch *m;

char *url = "http://odkit.sourceforge.net";

re = odk_regex_new("(http|ftp)://([./]+)", 0, 0);

m = odk_regex_match(re, url, 0);

memset(proto, 0, 5);
memcpy(proto, (char*)((int)url + m->submatch[0].start), 
    m->submatch[0].end - m->submatch[0].start);

memset(path, 0, 200);
memcpy(path, (char*)((int)url + m->submatch[1].start), 
    m->submatch[1].end - m->submatch[1].start);

printf("Protocol: %s\n", proto);
printf("Path: %s\n", path);

odk_regex_free(re);
odk_match_free(m);

\end{verbatim}

In this example we create a regex object, then we match the pattern on the url, staring
from the first character (0), this will return a odkRegMatch object, then we can use the
match information about the offsets of the pattern to copy those characters to other
strings.

Finally we must free all memory used, odkUtils includes two convenient functions, 
odk\_regex\_free for odkRegex object and odk\_match\_free for odkRegMatch objects.

\subsection{Regular expresion matches}

The odkUtils regex API has the odkRegMatch structure, which holds the index of the
first character where the pattern was found (start) and the last one (end), it may
also have submatches (odkRegSubMatch).

Actual structure is something like this (see on the source headers):

\begin{verbatim}
typedef struct{
        int start;
        int end;
}odkRegSubMatch;

typedef struct{
        int start;
        int end;
        int count;
        odkRegSubMatch *submatch;
} odkRegMatch;
\end{verbatim}

In both structures start and end are offsets.

Additionally odkUtils includes a function to capture al matches in a string, this
function will return a singly linked list:

\begin{verbatim}
odkRegMatch *m;
odkSList *list;

list = odk_regex_match(re, somestring);

for SLIST_ITERATOR(m, list, odkRegMatch*){
    ...
}

odk_slist_free(list, (FREE_FUNC)&odk_match_free);
\end{verbatim}

You may get the list and then iterate it, you can use the odk\_match\_free function
to free all the list elements too.

\end{document}
