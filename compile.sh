#!/bin/bash
if [ -n $1 ]; then
if [ "$1" = "install" ]; then
	PREFIX=`cat defs`
	if [ -n "$2" ]; then
		PREFIX="$2"
	fi

	INCPATH="$PREFIX/include/odk"
	LIBPATH="$PREFIX/lib"
	
	mkdir -p $PREFIX
	mkdir -p $INCPATH
	mkdir -p $LIBPATH
	mkdir -p $PREFIX/bin
	echo "Installing: Libraries"
	cp bin/*.so $LIBPATH
	cp bin/*.a $LIBPATH
	cp bin/odkutils-config $PREFIX/bin
	ldconfig
	echo "Installing: Development Headers"
	mkdir -p $INCPATH/odkutils
	cp include/odkutils/*.h $INCPATH/odkutils/
	cp include/*.h $INCPATH/
	exit
fi
fi

if [ -n "$1" ]; then
	PREFIX="$1"
else
	PREFIX="/usr"
fi

VERSION="0.5.8"
	
INCPATH="$PREFIX/include/odk"
LIBPATH="$PREFIX/lib"
		
rm -f config.h
rm -f dltest
rm -f dltest.c

echo "
#include <malloc.h>
#include <stdio.h>
#include <dlfcn.h>

int main(){
	void *d;
	
	d = dlopen(\"c.so\", RTLD_LAZY);
	
	return 1;
}

" > dltest.c

if (gcc dltest.c -o dltest 2>/dev/null >/dev/null); then
	DL=""
else
	DL="-ldl"
fi

rm -f dltest
rm -f dltest.c

rm -fr bin	
mkdir -p bin
export CFLAGS="-fPIC -g"

echo "Compiling ODK Utils"

echo " Compiling Sources"
gcc src/common.c -I./include $CFLAGS -c -o bin/common.o
gcc src/logs.c -I./include $CFLAGS -c -o bin/logs.o
gcc src/bitfield.c -I./include $CFLAGS -c -o bin/bitfield.o
gcc src/mempool.c -I./include $CFLAGS -c -o bin/mempool.o
gcc src/list.c -I./include $CFLAGS -c -o bin/list.o
gcc src/slist.c -I./include $CFLAGS -c -o bin/slist.o
gcc src/hashtable.c -I./include $CFLAGS -c -o bin/hashtable.o
gcc src/configstore.c -I./include $CFLAGS -c -o bin/configstore.o

gcc src/xml.c -I./include $CFLAGS -c -o bin/xml.o
gcc src/xmlwrite.c -I./include $CFLAGS -c -o bin/xmlwrite.o
gcc src/xmlparse.c -I./include $CFLAGS -c -o bin/xmlparse.o
gcc src/xmlconfig.c -I./include $CFLAGS -c -o bin/xmlconfig.o

gcc src/dynload-posix.c -I./include $CFLAGS -c -o bin/dynload.o
gcc src/mutex-posix.c -I./include $CFLAGS -c -o bin/mutex.o
gcc src/thread-posix.c -I./include $CFLAGS -c -o bin/thread.o
gcc src/regex-pcre.c -I./include `pcre-config --cflags` $CFLAGS -c -o bin/regex.o

gcc src/object.c -I./include $CFLAGS -c -o bin/object.o
gcc src/path.c -I./include $CFLAGS -c -o bin/path.o
gcc src/appconfig.c -I./include $CFLAGS -c -o bin/appconfig.o

echo " Creating Library"

echo "   * odkUtils "
gcc bin/bitfield.o bin/mempool.o bin/list.o bin/slist.o bin/thread.o bin/mutex.o bin/dynload.o bin/regex.o bin/appconfig.o bin/path.o bin/hashtable.o bin/common.o bin/logs.o bin/object.o bin/configstore.o -lpthread -shared -Wl,-soname,libodkutils.so $CFLAGS $DL -o bin/libodkutils.so
ar rc bin/libodkutils.a bin/bitfield.o bin/mempool.o bin/list.o bin/slist.o bin/thread.o bin/mutex.o bin/dynload.o bin/regex.o bin/appconfig.o bin/path.o bin/hashtable.o bin/common.o bin/logs.o bin/object.o bin/configstore.o
ranlib bin/libodkutils.a


echo "   * odkUtils XML "
if [ -a libexpat.a ]; then
	XMLLIBS=""
	echo "     Building with static expat"
	gcc bin/xml.o bin/xmlwrite.o bin/xmlparse.o bin/xmlconfig.o ./libexpat.a -shared -Wl,-soname,libodkutils_xml.so $CFLAGS `pcre-config --cflags --libs` -o bin/libodkutils_xml.so
	ar x libexpat.a
	ar rc bin/libodkutils_xml.a bin/xml.o bin/xmlwrite.o bin/xmlparse.o bin/xmlconfig.o xmlparse.o xmlrole.o xmltok.o
	ranlib bin/libodkutils_xml.a
else
	XMLLIBS="-lexpat"
	gcc bin/xml.o bin/xmlwrite.o bin/xmlparse.o bin/xmlconfig.o -shared -Wl,-soname,libodkutils_xml.so $CFLAGS -o bin/libodkutils_xml.so
	ar rc bin/libodkutils_xml.a bin/xml.o bin/xmlwrite.o bin/xmlparse.o bin/xmlconfig.o
	ranlib bin/libodkutils_xml.a
fi

echo $PREFIX > defs

echo "static char* DL = \"$DL\";" >> config.h
echo "static char* PREFIX = \"$PREFIX\";" >> config.h
echo "static char* VERSION = \"$VERSION\";" >> config.h
echo "static char* INCLUDEPATH = \"$INCPATH\";" >> config.h
echo "static char* LIBPATH = \"$LIBPATH\";" >> config.h
echo "static char* XMLLIBS = \"$XMLLIBS\";" >> config.h

echo " Creating odkutils-config Script"
gcc config.c -I./ -o bin/odkutils-config

