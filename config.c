#include <stdio.h>
#include <string.h>
#include "config.h"

void
usage ()
{
	printf ("Usage: odkda-config [OPTION]\n\n");
	printf ("Accepted values for OPTION are:\n\n");
	printf ("  --prefix          Print installation prefix\n");
	printf ("  --static          Print C library static linking information\n");
	printf ("  --libs            Print C library linking information\n");
	printf
		("  --libs-utils      Print C library linking information (base utilities only)\n");
	printf
		("  --libs-interop    Print C library linking information (interop utilties only)\n");
	printf
		("  --libs-xml        Print C library linking information (xml utilties only)\n");
	printf ("  --cflags          Print C pre-processor and compiler flags\n");
	printf ("  --version         Output version information\n");
}

int
main (int argc, char **argv)
{
	int i;
	int s = 0;

	if (argc == 1) {
		usage ();
		return 0;
	}

	for (i = 1; i < argc; i++) {
		if (!strcmp ("--static", argv[i])) {
			s = 1;
			i = argc + 1;
		}
	}

	for (i = 1; i < argc; i++) {
		if (!strcmp ("--libs", argv[i])) {
			if (s) {
				printf
					(" %s/libodkutils_xml.a %s/libodkutils.a -lpcre %s %s ",
					 LIBPATH, LIBPATH, DL, XMLLIBS);
			} else {
				printf
					(" -lodkutils -lpcre -lodkutils_xml %s %s ",
					 DL, XMLLIBS);
			}
		} else if (!strcmp ("--libs-utils", argv[i])) {
			if (s) {
				printf (" %s/libodkutils.a %s ", LIBPATH, DL);
			} else {
				printf (" -lodkutils -lpcre %s ", DL);
			}
		} else if (!strcmp ("--libs-interop", argv[i])) {
			if (s) {
				printf (" %s ", DL);
			} else {
				printf (" %s ", DL);
			}
		} else if (!strcmp ("--libs-xml", argv[i])) {
			if (s) {
				printf (" %s/libodkutils_xml.a %s/libodkutils.a %s %s ",
						LIBPATH, LIBPATH, XMLLIBS, DL);
			} else {
				printf (" -lodkutils -lodkutils_xml %s %s ", XMLLIBS, DL);
			}
		} else if (!strcmp ("--cflags", argv[i])) {
			printf (" -fPIC -I%s -L%s ", INCLUDEPATH, LIBPATH);
		} else if (!strcmp ("--prefix", argv[i])) {
			printf ("%s", PREFIX);
		} else if (!strcmp ("--version", argv[i])) {
			printf ("%s", VERSION);
		} else if (!strcmp ("--static", argv[i])) {
		} else {
			usage ();
			return 0;
		}
	}

	printf ("\n");
	return 0;
}
