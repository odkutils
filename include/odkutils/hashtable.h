/*
odkUtils: Utility Library
        Generic Hash Table
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _HASHTABLE_H
#define _HASHTABLE_H

typedef struct {
	char *key;
	void *value;
} odkHashEl;

typedef struct {
	int count;
	odkSList *buckets[256];
} odkHashTable;

odkHashTable *odk_hashtable_new (int mempool);
int odk_hashtable_insert (odkHashTable * ht, char *key, void *value);
void *odk_hashtable_lookup (odkHashTable * ht, char *key);
void odk_hashtable_free (odkHashTable * ht, FREE_FUNC * free_func);

#endif
