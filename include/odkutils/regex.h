/*
odkUtils: Utility Library
        Regular Expresions
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#define REGEX_MULTILINE			2
#define REGEX_NEWLINE_CR		4
#define REGEX_NEWLINE_LF		8
#define REGEX_NEWLINE_CRLF		16
#define REGEX_NEWLINE_ANYCRLF	32
#define REGEX_NEWLINE_ANY		64
#define REGEX_CASELESS			128
#define REGEX_DOTALL			256
#define REGEX_STUDY				512

typedef struct{
	int start;
	int end;
}odkRegSubMatch;

typedef struct{
	int start;
	int end;
	int count;
	odkRegSubMatch *submatch;
} odkRegMatch;

typedef struct odkRegex_s odkRegex;

/* @FUNC: odk_regex_new
	@DESC: Creates a new regular expression object
	@PARAM[pattern]: Regular expression pattern.
	@PARAM[options]: Pattern compilation options.
	@PARAM[scount]: Expected submatches, by default 5 are allocated if scount <= 5.
	@RETS: A Regex object, NULL on error.
*/
odkRegex *odk_regex_new(char *pattern, int options, int scount);

/* @FUNC: odk_regex_match
	@DESC: Returns a single match of the given pattern.
	@PARAM[re]: A valid Regex object.
	@PARAM[str]: A C string to search.
	@PARAM[pos]: Position to start searching.
	@RETS: A Match object, NULL on error.
*/
odkRegMatch *odk_regex_match(odkRegex *re, char *str, int pos);

/* @FUNC: odk_regex_match_all
	@DESC: Returns a all matches of the given pattern.
	@PARAM[re]: A valid Regex object.
	@PARAM[str]: A C string to search.
	@RETS: A list with Match objects.
*/
odkSList *odk_regex_match_all(odkRegex *re, char *str);

/* @FUNC: odk_regex_validate
	@DESC: Validates a given string agains a pattern.
	@PARAM[re]: A valid Regex object.
	@PARAM[str]: A C string to match.
	@RETS: True if the string validates, false otherwise..
*/
int odk_regex_validate(odkRegex *re, char *str);


/* @FUNC: odk_match_free
	@DESC: Frees the memory used by the Match object.
	@PARAM[m]: A valid Match object.
	@RETS: A Match object, NULL on error.
*/
void odk_match_free(odkRegMatch *m);

/* @FUNC: odk_regex_free
	@DESC: Frees the memory used by the Regex object.
	@PARAM[re]: A valid Regex object.
	@RETS: 
*/
void odk_regex_free(odkRegex *re);

/* @FUNC: odk_submatch_copy
	@DESC: Copy the submatch string
	@PARAM[str]: Source string.
    @PARAM[m]: Match object.
    @PARAM[n]: Number of submatch to use.
	@RETS: 
*/
char *odk_submatch_copy(char *str, odkRegMatch *m, int n);
