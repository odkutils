/*
odkUtils: Utility Library
        Cross-Platform Mutex
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _MUTEX_H
#define _MUTEX_H

typedef struct odkMutex_s odkMutex;

odkMutex *odk_mutex_new ();
void odk_mutex_destroy (odkMutex * mutex);

void odk_mutex_lock (odkMutex * mutex);
int odk_mutex_trylock (odkMutex * mutex);
void odk_mutex_unlock (odkMutex * mutex);

#endif
