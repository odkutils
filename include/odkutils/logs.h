/*
odkUtils: Utility Library
        Logging Facility
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _LOGS_H
#define _LOGS_H

#define LOG_OFF		0
#define LOG_ERRORS	1
#define LOG_WARNING	2
#define LOG_DEBUG	3

#define LOG_EXIT_ONERROR	1
#define LOG_SETTIME			2
#define LOG_XML				4

void odk_log_init (char *file, char level, char flags);
void odk_log_level (char level);

void odk_log_print (char *str);
void odk_log_printf (char *format, ...);

void odk_log_debug (char *format, ...);
void odk_log_warning (char *format, ...);
void odk_log_error (char *format, ...);

#endif
