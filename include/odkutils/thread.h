/*
odkUtils: Utility Library
        Thread Abstraction
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _THREAD_H
#define _THREAD_H

typedef struct odkThread_s odkThread;

odkThread *odk_thread_create (void *(*thread_rutine) (void *), void *arg);
void odk_thread_exit (void *r);
void odk_thread_join (odkThread * t);
void odk_thread_detach (odkThread * t);

#endif
