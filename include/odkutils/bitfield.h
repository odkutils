/*
odkUtils: Utility Library
        Bit Fields
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _BITFIELD_H
#define _BITFIELD_H

typedef unsigned char *odkBitfield;

/* @FUNC: odk_bitfield_new
	@DESC: odkBitfield constructor
	@PARAM[n]: Number of bits to allocate.
	@RETS: a valid odkBitfield object, NULL otherwise.
*/
odkBitfield odk_bitfield_new (int n);

/* @FUNC: odk_bitfield_resize
	@DESC: Resizes the number of allocated bits on a bitfield.
	@PARAM[b]: A valid odkBitfield object.
	@PARAM[n]: Number of bits to allocate.
	@RETS: a valid odkBitfield object, NULL otherwise.
*/
odkBitfield odk_bitfield_resize (odkBitfield b, int n);

/* @FUNC: odk_bitfield_set
	@DESC: Turns on a given bit.
	@PARAM[b]: A valid odkBitfield object.
	@PARAM[n]: Number of the bit to set.
	@RETS: 
*/
void odk_bitfield_set (odkBitfield b, int n);

/* @FUNC: odk_bitfield_unset
	@DESC: Turns off a given bit.
	@PARAM[b]: A valid odkBitfield object.
	@PARAM[n]: Number of the bit to set.
	@RETS: 
*/
void odk_bitfield_unset (odkBitfield b, int n);

/* @FUNC: odk_bitfield_clear
	@DESC: Turns off all the bits on the bitfield.
	@PARAM[b]: A valid odkBitfield object.
	@RETS: 
*/
void odk_bitfield_clear (odkBitfield b);

/* @FUNC: odk_bitfield_get
	@DESC: Returns the value of a given bit.
	@PARAM[b]: A valid odkBitfield object.
	@PARAM[n]: Number of the bit to get.
	@RETS: The value of the given field.
*/
int odk_bitfield_get (odkBitfield b, int n);

#endif
