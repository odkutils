/*
odkUtils: Utility Library
        Memory Pools
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _MEMPOOL_H
#define _MEMPOOL_H

typedef struct _odkMemPool odkMemPool;

struct _odkMemPool {
	int objSize;
	int maxCount;
	int objCount;
	int refCount;
	odkMemPool *next;
	void *memPool;
};

odkMemPool *odk_mempool_new (int objSize, int maxCount);
void odk_mempool_free (odkMemPool * pool);

void *odk_mempool_alloc (odkMemPool * pool, int objSize);
void odk_mempool_dealloc (odkMemPool * pool, void *ptr);

#endif
