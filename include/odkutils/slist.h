/*
odkUtils: Utility Library
        Single Linked Lists
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _SLIST_H
#define _SLIST_H

typedef struct _odkSList odkSList;
typedef struct _odkSNode odkSNode;

struct _odkSNode {
	odkSNode *next;
	void *data;
};

struct _odkSList {
	odkSNode *first;
	odkSNode *last;
	odkSNode *current;
	int count;
	int cindex;
	odkMemPool *memPool;
};

/* @FUNC: odk_slist_new
	@DESC: Creates a new empty list with optional memory pool.
	@PARAM[mempool]: Number of elements to preallocate, it can be 0 to disable memory pools.
	@RETS: A list object.
*/
odkSList *odk_slist_new (int mempool);

/* @FUNC: odk_slist_new_shared
	@DESC: Creates a new empty list sharing an existing memory pool.
	@PARAM[mempool]: A valid memory pool.
	@RETS: A list object.
*/
odkSList *odk_slist_new_shared (odkMemPool * mempool);

/* @FUNC: odk_slist_free
	@DESC: Frees a list and its objects.
	@PARAM[list]: A valid list object.
	@PARAM[func]: Function to free each element data, can be NULL and wont free data.
	@RETS: 
*/
void odk_slist_free (odkSList * list, FREE_FUNC func);

/* @FUNC: odk_slist_empty
	@DESC: Frees all the elements of the list.
	@PARAM[list]: A valid list object.
	@PARAM[func]: Function to free each element data, can be NULL and wont free data.
	@RETS: 
*/
void odk_slist_empty (odkSList * list, FREE_FUNC func);

#ifdef NOMACROS
#define odk_slist_count _odk_slist_count
#define odk_slist_data _odk_slist_data
#define odk_slist_eof _odk_slist_eof
#else
#define odk_slist_count(list) ((list == NULL) ? 0 : list->count)
#define odk_slist_data(list) (((list == NULL) || (list->current == NULL)) ? NULL : list->current->data)
#define odk_slist_eof(list) (((list == NULL) || (list->current == NULL)) ? true : false)
#endif

/* @FUNC: odk_slist_append
	@DESC: Adds an element to the end of the list.
	@PARAM[list]: A valid list object.
	@PARAM[data]: element data.
	@RETS: 1 if sucessful.
*/
bool odk_slist_append (odkSList * list, void *data);

/* @FUNC: odk_slist_preappend
	@DESC: Adds an element to the beginning of the list.
	@PARAM[list]: A valid list object.
	@PARAM[data]: element data.
	@RETS: 1 if sucessful.
*/
bool odk_slist_preappend (odkSList * list, void *data);

/* @FUNC: odk_slist_data
	@DESC: Gets current element data.
	@PARAM[list]: A valid list object.
	@RETS: Current element data.
*/
void *_odk_slist_data (odkSList * list);

/* @FUNC: odk_slist_next
	@DESC: Moves the list cursor to next element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_slist_next (odkSList * list);

/* @FUNC: odk_slist_first
	@DESC: Moves the list cursor to first element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_slist_first (odkSList * list);

/* @FUNC: odk_slist_last
	@DESC: Moves the list cursor to last element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_slist_last (odkSList * list);

/* @FUNC: odk_slist_nth
	@DESC: Moves the list cursor to a given element.
	@PARAM[list]: A valid list object.
	@PARAM[n]: Number of element to move the cursor to.
	@RETS: 1 if sucessful.
*/
bool odk_slist_nth(odkSList *list, int n);

/* @FUNC: odk_slist_delete
	@DESC: Deletes the current element, will not free data.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_slist_delete (odkSList * list);

/* @FUNC: odk_slist_count
	@DESC: Returns the list element count.
	@PARAM[list]: A valid list object.
	@RETS: List element count.
*/
int _odk_slist_count (odkSList * list);

/* @FUNC: odk_slist_eof
	@DESC: Return true if cursor is a the end of the list.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool _odk_slist_eof (odkSList * list);

/* @FUNC: odk_slist_find
	@DESC: Iterates a list from the beggining comparing d using func.
	@PARAM[list]: A valid list object.
	@PARAM[d]: data for comparision.
	@PARAM[func]: A comparision function for specific data type, should return true if both elements are equal.
	@RETS: The first element data where comparation returns true.
*/
void *odk_slist_find (odkSList * list, void *d, LISTFIND_FUNC func);
void *odk_slist_find_string (odkSList * list, char *d);

/* @FUNC: odk_slist_toarray
	@DESC: Converts a list to a fixed array of pointers.
	@PARAM[list]: A valid list object.
	@RETS: An array of pointers, NULL on error.
*/
void **odk_slist_toarray (odkSList * list);

/* @FUNC: odk_slist_index
	@DESC: Returns the position of the list cursor.
	@PARAM[list]: A valid list object.
	@RETS: Cursor position.
*/
int odk_slist_index (odkSList *list);

/* @FUNC: odk_slist_is_first
	@DESC: Return true if cursor is on the first element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
int odk_slist_is_first (odkSList *list);

/* @FUNC: odk_slist_is_last
	@DESC: Return true if cursor is a the end of the list.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
int odk_slist_is_last (odkSList *list);

/* @FUNC: odk_slist_pop
	@DESC: Return an element data and deletes it.
	@PARAM[list]: A valid list object.
	@PARAM[last]: If true will pop the last element, otherwise the first element.
	@RETS: Element data.
*/
void *odk_slist_pop(odkSList *list, int last);

/* @FUNC: odk_slist_push
	@DESC: Pushes an element to the list.
	@PARAM[list]: A valid list object.
	@PARAM[last]: If true will push the element to the last (append), otherwise to the first (preappend).
	@RETS: 1 if sucessful.
*/
int odk_slist_push(odkSList *list, void *data, int last);

#define for_slist(b) for (odk_slist_first(b); !_odk_slist_eof(b); odk_slist_next(b))

/* @FUNC: SLIST_ITERATOR
	@DESC: List iterator macro.
	@PARAM[obj]: Variable that will recive the element data.
	@PARAM[list]: A valid list object.
	@PARAM[cast]: Type casting.
	@RETS:
*/
#define SLIST_ITERATOR(obj, lst, cast) (odk_slist_first(lst); !_odk_slist_eof(lst), obj =(cast)odk_slist_data(lst); odk_slist_next(lst))

#endif
