/*
odkUtils: Utility Library
        Cross-Platform Dynamic Library Loading
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _DYNLOAD_H
#define _DYNLOAD_H

typedef struct odkLibHandle_s odkLibHandle;

/* @FUNC: odk_library_open
	@DESC: Loads a given dinamic library (shared object)
	@PARAM[lib]: The library name without extension.
	@RETS: A new library handle.
*/
odkLibHandle *odk_library_open (char *lib);

/* @FUNC: odk_library_symbol
	@DESC: Loads a given symbol from a library handle
	@PARAM[lib]: A valid library handle (opened)
	@PARAM[symbol]: The symbol name.
	@RETS: A pointer to the given symbol.
*/
void *odk_library_symbol (odkLibHandle *lib, char *symbol);

/* @FUNC: odk_library_close
	@DESC: Closes a given library handle
	@PARAM[lib]: A valid library handle (opened)
	@RETS: 
*/
void odk_library_close (odkLibHandle *lib);

#endif
