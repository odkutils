/*
odkUtils: Utility Library
        Doubly Linked Lists
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _LIST_H
#define _LIST_H

typedef struct _odkList odkList;
typedef struct _odkNode odkNode;

struct _odkNode {
	odkNode *next;
	odkNode *prev;
	void *data;
};

struct _odkList {
	odkNode *first;
	odkNode *last;
	odkNode *current;
	int count;
	int cindex;
	odkMemPool *memPool;
};

/* @FUNC: odk_list_new
	@DESC: Creates a new empty list with optional memory pool.
	@PARAM[mempool]: Number of elements to preallocate, it can be 0 to disable memory pools.
	@RETS: A list object.
*/
odkList *odk_list_new (int mempool);

/* @FUNC: odk_list_new_shared
	@DESC: Creates a new empty list sharing an existing memory pool.
	@PARAM[mempool]: A valid memory pool.
	@RETS: A list object.
*/
odkList *odk_list_new_shared (odkMemPool * mempool);

/* @FUNC: odk_list_free
	@DESC: Frees a list and its objects.
	@PARAM[list]: A valid list object.
	@PARAM[func]: Function to free each element data, can be NULL and wont free data.
	@RETS: 
*/
void odk_list_free (odkList * list, FREE_FUNC func);

/* @FUNC: odk_list_empty
	@DESC: Frees all the elements of the list.
	@PARAM[list]: A valid list object.
	@PARAM[func]: Function to free each element data, can be NULL and wont free data.
	@RETS: 
*/
void odk_list_empty (odkList * list, FREE_FUNC func);

#ifdef NOMACROS
#define odk_list_count _odk_list_count
#define odk_list_data _odk_list_data
#define odk_list_bof _odk_list_bof
#define odk_list_eof _odk_list_eof
#else
#define odk_list_count(list) ((list == NULL) ? 0 : list->count)
#define odk_list_data(list) (((list == NULL) || (list->current == NULL)) ? NULL : list->current->data)
#define odk_list_eof(list) (((list == NULL) || (list->current == NULL)) ? true : false)
#define odk_list_bof(list) (((list == NULL) || (list->current == NULL) || (list->current->prev == NULL)) ? false : true)
#endif


/* @FUNC: odk_list_delete
	@DESC: Deletes the current element, will not free data.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_list_delete (odkList * list);

/* @FUNC: odk_list_insert
	@DESC: Inserts an element after current element and before next element.
	@PARAM[list]: A valid list object.
	@PARAM[data]: element data.
	@RETS: 1 if sucessful.
*/
bool odk_list_insert (odkList * list, void *data);

/* @FUNC: odk_list_append
	@DESC: Adds an element to the end of the list.
	@PARAM[list]: A valid list object.
	@PARAM[data]: element data.
	@RETS: 1 if sucessful.
*/
bool odk_list_append (odkList * list, void *data);

/* @FUNC: odk_list_preappend
	@DESC: Adds an element to the beginning of the list.
	@PARAM[list]: A valid list object.
	@PARAM[data]: element data.
	@RETS: 1 if sucessful.
*/
bool odk_list_preappend (odkList * list, void *data);

/* @FUNC: odk_list_data
	@DESC: Gets current element data.
	@PARAM[list]: A valid list object.
	@RETS: Current element data.
*/
void *_odk_list_data (odkList * list);

/* @FUNC: odk_list_next
	@DESC: Moves the list cursor to next element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_list_next (odkList * list);

/* @FUNC: odk_list_prev
	@DESC: Moves the list cursor to previous element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_list_prev (odkList * list);

/* @FUNC: odk_list_first
	@DESC: Moves the list cursor to first element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_list_first (odkList * list);

/* @FUNC: odk_list_last
	@DESC: Moves the list cursor to last element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool odk_list_last (odkList * list);

/* @FUNC: odk_list_nth
	@DESC: Moves the list cursor to a given element.
	@PARAM[list]: A valid list object.
	@PARAM[n]: Number of element to move the cursor to.
	@RETS: 1 if sucessful.
*/
bool odk_list_nth(odkList *list, int n);

/* @FUNC: odk_list_count
	@DESC: Returns the list element count.
	@PARAM[list]: A valid list object.
	@RETS: List element count.
*/
int _odk_list_count (odkList * list);

/* @FUNC: odk_list_bof
	@DESC: Return true if cursor is a the beggining of the list.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool _odk_list_bof (odkList * list);

/* @FUNC: odk_list_eof
	@DESC: Return true if cursor is a the end of the list.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
bool _odk_list_eof (odkList * list);

/* @FUNC: odk_list_find
	@DESC: Iterates a list from the beggining comparing d using func.
	@PARAM[list]: A valid list object.
	@PARAM[d]: data for comparision.
	@PARAM[func]: A comparision function for specific data type, should return true if both elements are equal.
	@RETS: The first element data where comparation returns true.
*/
void *odk_list_find (odkList * list, void *d, LISTFIND_FUNC func);
void *odk_list_find_string (odkList * list, char *d);

/* @FUNC: odk_list_toarray
	@DESC: Converts a list to a fixed array of pointers.
	@PARAM[list]: A valid list object.
	@RETS: An array of pointers, NULL on error.
*/
void **odk_list_toarray (odkList * list);

/* @FUNC: odk_list_index
	@DESC: Returns the position of the list cursor.
	@PARAM[list]: A valid list object.
	@RETS: Cursor position.
*/
int odk_list_index (odkList *list);

/* @FUNC: odk_list_is_first
	@DESC: Return true if cursor is on the first element.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
int odk_list_is_first (odkList *list);

/* @FUNC: odk_list_is_last
	@DESC: Return true if cursor is a the end of the list.
	@PARAM[list]: A valid list object.
	@RETS: 1 if sucessful.
*/
int odk_list_is_last (odkList *list);

/* @FUNC: odk_list_pop
	@DESC: Return an element data and deletes it.
	@PARAM[list]: A valid list object.
	@PARAM[last]: If true will pop the last element, otherwise the first element.
	@RETS: Element data.
*/
void *odk_list_pop(odkList *list, int last);

/* @FUNC: odk_list_push
	@DESC: Pushes an element to the list.
	@PARAM[list]: A valid list object.
	@PARAM[last]: If true will push the element to the last (append), otherwise to the first (preappend).
	@RETS: 1 if sucessful.
*/
int odk_list_push(odkList *list, void *data, int last);


/* @FUNC: odk_list_quicksort
	@DESC: Sorts the list elements.
	@PARAM[list]: A valid list object.
	@PARAM[ocmp]: A comparision funcion for list elements data.
	@RETS: 1 if sucessful.
*/
int odk_list_quicksort(odkList *list, CMP_FUNC ocmp);

#define for_list(b) for (odk_list_first(b); !_odk_list_eof(b); odk_list_next(b))


/* @FUNC: LIST_ITERATOR
	@DESC: List iterator macro.
	@PARAM[obj]: Variable that will recive the element data.
	@PARAM[list]: A valid list object.
	@PARAM[cast]: Type casting.
	@RETS:
*/
#define LIST_ITERATOR(obj, lst, cast) (odk_list_first(lst); !_odk_list_eof(lst), obj =(cast)odk_list_data(lst); odk_list_next(lst))

#endif

