/*
odkUtils: Utility Library
        Structures to help manipulate XML
		XML Writer
		XML Parser based on expat
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ODK_XML_H
#define _ODK_XML_H

typedef struct _odkXMLNode odkXMLNode;

struct _odkXMLNode {
	char *name;
	char *data;
	char cdata;
	odkSList *attribs;
	odkSList *children;
	struct _odkXMLNode *parent;
};

/* @FUNC: odk_xml_node_new
	@DESC: Creates a new XML node.
	@PARAM[parent]: XML Node parent, can be NULL for no parent.
	@PARAM[name]: A C string for the node tag name.
	@RETS: A XMLNode object, NULL on error.
*/
odkXMLNode *odk_xml_node_new (odkXMLNode * parent, char *name);

/* @FUNC: odk_xml_attrib_get
	@DESC: Returns a Node attribute
	@PARAM[n]: A valid XML Node object.
	@PARAM[attrib]: Attribute name.
	@RETS: A string with the value of the attribute, NULL on error or no attribute.
*/
char *odk_xml_attrib_get (odkXMLNode * n, char *attrib);

/* @FUNC: odk_xml_attrib_set
	@DESC: Sets a Node attribute
	@PARAM[n]: A valid XML Node object.
	@PARAM[attrib]: Attribute name.
	@PARAM[value]: Attribute value.
	@RETS: 
*/
void odk_xml_attrib_set (odkXMLNode * n, char *attrib, char *value);

/* @FUNC: odk_xml_data_get
	@DESC: Returns a Node data.
	@PARAM[n]: A valid XML Node object.
	@RETS: A string with the data of the node, NULL on error or no data.
*/
char *odk_xml_data_get (odkXMLNode * n);

/* @FUNC: odk_xml_data_set
	@DESC: Sets a Node data.
	@PARAM[n]: A valid XML Node object.
	@PARAM[data]: Node data.
	@RETS: 
*/
void odk_xml_data_set (odkXMLNode * n, char *data);

/* @FUNC: odk_xml_has_children
	@DESC: Checks if the node has children nodes.
	@PARAM[n]: A valid XML Node object.
	@RETS: True if node has children, False otherwise.
*/
int odk_xml_has_children(odkXMLNode *n);

/* @FUNC: odk_xml_children
	@DESC: Returns a list of node children.
	@PARAM[n]: A valid XML Node object.
	@RETS: A SList object with child nodes.
*/
odkSList *odk_xml_children (odkXMLNode * n);

/* @FUNC: odk_xml_root
	@DESC: Returns the root node of the tree.
	@PARAM[n]: A valid XML Node object.
	@RETS: A XMLNode.
*/
odkXMLNode *odk_xml_root (odkXMLNode * n);

/* @FUNC: odk_xml_name
	@DESC: Returns the node tag name.
	@PARAM[n]: A valid XML Node object.
	@RETS: String with the node tag name.
*/
char *odk_xml_name (odkXMLNode * n);

/* @FUNC: odk_xml_child_get
	@DESC: Gets a given children.
	@PARAM[n]: A valid XML Node object.
	@PARAM[childname]: Children tag name.
	@RETS: A XMLNode.
*/
odkXMLNode *odk_xml_child_get (odkXMLNode * n, char *childname);

/* @FUNC: odk_xml_reparent
	@DESC: Reparents a node.
	@PARAM[parent]: A valid XML Node object which will be the node parent.
	@PARAM[child]: The node we want to reparent.
	@RETS: 
*/
void odk_xml_reparent (odkXMLNode * parent, odkXMLNode * child);

/* @FUNC: odk_xml_free
	@DESC: Frees the node data and it's childs.
	@PARAM[n]: A valid XML Node object.
	@RETS: A XMLNode.
*/
void odk_xml_free (odkXMLNode * n);

/* @FUNC: odk_xml_parse
	@DESC: Parses an XML file and constructs a Node tree.
	@PARAM[file]: File name.
	@RETS: A valid XMLNode.
*/
odkXMLNode *odk_xml_parse (char *file);

/* @FUNC: odk_xml_write
	@DESC: Writes the node tree data to an XML file.
	@PARAM[file]: File name.
	@PARAM[n]: A valid XML Node.
	@RETS: 1 if sucessful.
*/
int odk_xml_write (char *file, odkXMLNode * n);

/* @FUNC: odk_xml_path
	@DESC: Returns a Node from given path.
	@PARAM[n]: Node to use as root.
	@PARAM[path]: Node path to traverse.
	@RETS: A XML node on the given path, NULL on error.
*/
odkXMLNode *odk_xml_path (odkXMLNode * n, char *path);

/* @FUNC: odk_xml_path_data_get
	@DESC: Returns the data of a Node from given path.
	@PARAM[n]: Node to use as root.
	@PARAM[path]: Node path to traverse.
	@RETS: Node data from path, NULL on error or no data.
*/
#define odk_xml_path_data_get(n, path) odk_xml_data_get(odk_xml_path(n, path))

/* @FUNC: odk_xml_path_data_set
	@DESC: Sets the data of a Node from given path.
	@PARAM[n]: Node to use as root.
	@PARAM[path]: Node path to traverse.
	@PARAM[value]: Value for the node.
	@RETS: 1 if sucessful.
*/
int odk_xml_path_data_set (odkXMLNode * n, char *path, char *value);

int odk_xml_delete(odkXMLNode *n, odkXMLNode *d);
#endif
