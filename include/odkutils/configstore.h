/*
odkUtils: Utility Library
        Configuration Storing Facility
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _CONFIGSTORE_H
#define _CONFIGSTORE_H

#define KEY_TEXT		1
#define KEY_INTEGER		2
#define KEY_FLOAT		3
#define KEY_DIR			4

typedef char *(ODKAPI *FUNC_CONF_GETKEY) (void *cp, char *key);
typedef void (ODKAPI *FUNC_CONF_SETKEY) (void *cp, char *key, char *val);
typedef int (ODKAPI *FUNC_CONF_GETKEYI) (void *cp, char *key);
typedef void (ODKAPI *FUNC_CONF_SETKEYI) (void *cp, char *key, int val);
typedef float (ODKAPI *FUNC_CONF_GETKEYF) (void *cp, char *key);
typedef void (ODKAPI *FUNC_CONF_SETKEYF) (void *cp, char *key, float val);
typedef odkSList *(ODKAPI *FUNC_CONF_KEYLIST) (void *cp, char *path);
typedef int (ODKAPI *FUNC_CONF_KEYTYPE) (void *cp, char *path);
typedef int (ODKAPI *FUNC_CONF_SAVE) (void *cp);
typedef int (ODKAPI *FUNC_CONF_LOAD) (void *cp);

typedef struct {
	FUNC_CONF_GETKEY get_key;
	FUNC_CONF_SETKEY set_key;
	FUNC_CONF_GETKEYI get_keyi;
	FUNC_CONF_SETKEYI set_keyi;
	FUNC_CONF_GETKEYF get_keyf;
	FUNC_CONF_SETKEYF set_keyf;
	FUNC_CONF_KEYLIST get_keylist;
	FUNC_CONF_KEYTYPE get_keytype;
	FUNC_CONF_SAVE save;
	FUNC_CONF_LOAD load;
} _ConfigStore;

#define CLASS_CONFIGSTORE CLASS_OBJECT _ConfigStore Class_ConfigStore;

typedef struct {
CLASS_CONFIGSTORE} odkConfigStore;

char *odk_config_key_get (odkConfigStore * cs, char *key);
int odk_config_key_set (odkConfigStore * cs, char *key, char *value);

int odk_config_key_geti (odkConfigStore * cs, char *key);
int odk_config_key_seti (odkConfigStore * cs, char *key, int value);

float odk_config_key_getf (odkConfigStore * cs, char *key);
int odk_config_key_setf (odkConfigStore * cs, char *key, float value);

odkSList *odk_config_key_list (odkConfigStore * cs, char *path);
int odk_config_key_type (odkConfigStore * cs, char *key);

int odk_config_save (odkConfigStore * cs);
int odk_config_load (odkConfigStore * cs);

#endif
