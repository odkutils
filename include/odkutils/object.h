/*
odkUtils: Utility Library
        Base Object Class and Model
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _OBJECT_H
#define _OBJECT_H

#define OBJECT_LOCK_SIMPLE	1
#define OBJECT_LOCK_SERIAL	2

typedef struct {
	int locked;
	char locktype;
	int refcount;
	char *className;
	odkMutex *mutex;
	odkSList *destructors;
	odkSList *instanceData;
} _Object;

#define CLASS_OBJECT _Object Class_Object;

typedef struct {
CLASS_OBJECT} Object;

#define OBJECT(o) (((Object*)o)->Class_Object)

void object_register (void *o, char *name);
void object_append_destructor (void *o, FREE_FUNC delfunc);

char *object_class (void *o);
void object_ref (void *o);
void object_unref (void *o);

int object_lock (void *o);
int object_trylock (void *o);
void object_unlock (void *o);

void object_set_lock_type (void *o, char l);

#endif
