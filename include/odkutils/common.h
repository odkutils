/*
odkUtils: Utility Library
        Common Defines and Functions
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ODKUTILS_COMMON_H
#define _ODKUTILS_COMMON_H

#ifdef WIN32
#ifdef ODKEXPORTS
    #define ODKDECL __declspec(dllexport)
#else
    #define ODKDECL __declspec(dllimport)
#endif
#else
#define ODKDECL 
#endif

#ifndef __cplusplus
#ifndef bool
typedef int bool;
#endif
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif


#ifdef WIN32
#include <windows.h>
#define ODKAPI __cdecl
#else
#define ODKAPI
#endif

typedef struct {
	char *key;
	void *value;
} odkKeyValue;

typedef void (ODKAPI * FREE_FUNC) (void *d);
typedef int (ODKAPI * LISTFIND_FUNC) (void *data1, void *data2);

/* Comparation of data1 and data2
	Must return 1 if data1 > data2
	Must return -1 if data1 < data2
	Must return 0 if data1 == data2
*/
typedef int (ODKAPI * CMP_FUNC) (void *data1, void *data2);

/* @FUNC: strcopy
	@DESC: Copies a given string
	@PARAM[string]: A C string to copy.
	@RETS: A new C string with a copy of the contents of the old one.
*/
char *strcopy (char *string);

/* @FUNC: strncopy
	@DESC: Copies a given string
	@PARAM[string]: A C string to copy.
	@PARAM[l]: Number of characters to copy.
	@RETS: A new C string with a copy of the contents of the old one.
*/
char *strncopy (char *string, int l);

#endif
