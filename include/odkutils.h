/*
odkUtils: Utility Library
        Basic Utility Set
		
Copyright (C) 2005 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ODKUTILS_H
#define _ODKUTILS_H

#if defined(_MSC_VER)
//#pragma comment(lib, \"libodkutils.lib\")
#endif

#ifdef __cplusplus
extern "C"{
#endif

#include <odkutils/common.h>
#include <odkutils/logs.h>
#include <odkutils/bitfield.h>
#include <odkutils/mempool.h>
#include <odkutils/list.h>
#include <odkutils/slist.h>
#include <odkutils/regex.h>
#include <odkutils/hashtable.h>
#include <odkutils/dynload.h>
#include <odkutils/thread.h>
#include <odkutils/mutex.h>
#include <odkutils/object.h>
#include <odkutils/configstore.h>
#include <odkutils/path.h>
#include <odkutils/appconfig.h>

#ifdef __cplusplus
}
#endif

#endif
