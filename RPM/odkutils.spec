#
# spec file for package odkUtils
#
# Copyright  (c)  2006  Carlos Daniel Ruvalcaba Valenzuela
#

Name: odkutils
Summary: The package provides a variety of utilities for cross-platform C programming
Version: 0.5.8
Release: 1
License: LGPL
Group: System/Database
URL: http://odkit.sourceforge.net/
Source: %{name}-%{version}.tar.gz
Provides: odkutils
Requires: expat
#Prereq: bash gcc
BuildPrereq: expat pcre
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description 
The package odkUtils provides a variety of utilities for cross-platform C programming

%package devel
Summary: odkUtils development files
Group: System/Database
Provides: odkutils-devel

%description devel
This package contains necessary header files for odkUtils development.

%prep 
%setup -q

%build 
sh compile.sh

%install 
rm -fr %{buildroot}
sh compile.sh install %{buildroot}/usr

%clean 
rm -fr %{buildroot}

%post 
/sbin/ldconfig

%files 
%defattr(-,root,root) 
%{_libdir}/*.so

%files devel
%defattr(-,root,root) 
%{_bindir}/*
%{_includedir}/* 
%{_libdir}/*.a

%changelog 

* Tue Jun 19 2007 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated to version 0.5.8

* Mon Apr 02 2007 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated to version 0.5.6

* Thu Dec 14 2006 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated minally descriptions
- Updated to latest version

* Mon Dec 11 2006 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- initial RPM spec
